MAKEFLAGS := --jobs=$(shell nproc)
SHELL := /bin/bash
# WARNING: EVERY LINE OF MAKEFILE RUN IN SEPARATE(ANOTHER SHELL)
all:
	@echo "Deploy script for my dotfiles"
	@echo "DONT USE SUDO IN BLOCK WHICH ALREADY HAVE SUDO ALIAS"
	@echo "Usage:"
	@echo "sh: make save # to save configs to dotfiles"
	@echo "sh: make push # to push configs"
	@echo "sh: make pacman pkgbase paru aurpkg nsxiv doas zsh gruvbox emacs kbd bluetooth xdg-dir tlp # to install base packages from repo/aur"
	@echo "sh: make bspwm     # to install bspwm desktop"
	@echo "sh: make suckless  # to install Suckless desktop"

### Packages ###
# Base #
BASEPKG := base base-devel zoxide fd rsync xorg mesa mesa-utils opendoas zsh git man-db tldr\
	   libx11 libxft xdotool xclip xorg xorg-server xorg-xinit xorg-xprop\
	   ffmpeg polkit opendoas\
	   artix-keyring chaotic-keyring archlinux-keyring
# Additional #
PACPKG := emacs p7zip maim brightnessctl bc yt-dlp mpv fzf dunst libnotify mediainfo\
	  zathura zathura-pdf-poppler zathura-ps zathura-cb zathura-djvu\
	  xdg-user-dirs xdg-utils\
	  pipewire pipewire-jack pipewire-docs pipewire-pulse pipewire-alsa wireplumber\
	  ttf-terminus-nerd moc-pulse ncdu
# Aur #
AURPKG := htim rm-improved lf-git picom-simpleanims-git gdown\
          lxappearance gruvbox-dark-gtk
### Aliases ###
PWD := $(shell pwd)
DOAS := doas
# Installs #
PACMAN := doas pacman --color always --needed --noconfirm -Sy
PARU := paru --needed --noconfirm -Sy
EVAL := emacs --batch --eval "(require 'org)" --eval
# Init #
RC_ON := doas rc-update add

test: save clean
	echo "Dont remove"

desktop: doas pacman pkgbase paru zram pkgadd lf xdg-dir nsxiv st zsh emacs bluetooth kbd tlp gruvbox

suckless: lf st #WORK!
	mkdir -p ~/.config/Suckless/x11
	cp -fvr ./home/.config/Suckless/ ~/.config/Suckless
	cp -fvr ./home/.config/X11/xinitrc_dwm ~/.config/X11/
	$(PACMAN) libxcb libexif imlib2 yajl jq nitrogen
	$(PARU) xkblayout-state-git
	$(DOAS) make -j4 install -C ~/.config/Suckless/x11/dwm/
	$(DOAS) make -j4 install -C ~/.config/Suckless/x11/dmenu/
	$(DOAS) make -j4 install -C ~/.config/Suckless/x11/scroll/
	$(DOAS) make -j4 install -C ~/.config/Suckless/x11/st/
	$(DOAS) make -j4 install -C ~/.config/Suckless/x11/dwmblocks-async/
bspwm: lf st #WORK!
	$(PACMAN) bspwm sxhkd polybar
	cp -fvr ./home/.config/bspwm ~/.config/bspwm
	cp -fvr ./home/.config/sxhkd ~/.config/sxhkd
	chmod +x ~/.config/bspwm/{bspwmrc,xinitrc_bspwm}
openbox: #WORK!
	$(PACMAN) openbox tint2
	cp -fvr ./home/.config/openbox ~/.config/openbox
	cp -fvr ./home/.config/tint2 ~/.config/tint2
lf: #WORK!
	$(PACMAN) xdg-utils eza rg ffmpegthumbnailer fd bat jq fzf xclip lf ueberzug perl-file-mimeinfo mediainfo
	cp -fvr ./home/.config/lf ~/.config/
	chmod +x ~/.config/lf/{cleaner,lfub,scope}
	xdg-mime default lf.desktop inode/directory
xdg-dir: #WORK!
	$(PACMAN) xdg-user-dirs xdg-utils
	cp -fvr ./home/.config/user-dirs.locale ~/.config/user-dirs.locale
	cp -fvr ./home/.config/mimeapps.list ~/.config/mimeapps.list
	cp -fvr ./home/.config/user-dirs.dirs ~/.config/user-dirs.dirs
	mkdir -p ~/docs/{.desc,dow,docs,etc,mus,pic,vid,dow}
nsxiv: #WORK!
	$(PACMAN) imlib2
	mkdir -p ~/.config/Suckless/x11
	cp -fvr ./home/.config/Suckless/x11/nsxiv ~/.config/Suckless/x11
	$(DOAS) make -j4 -C ~/.config/Suckless/x11/nsxiv
	$(DOAS) make -j4 install -C ~/.config/Suckless/x11/nsxiv
	cp -fvr ./home/.config/nsxiv ~/.config
	chmod +x ~/.config/nsxiv/exec/*
st: #WORK!
	mkdir -p ~/.config/Suckless/x11
	cp -fvr ./home/.config/Suckless/x11/st ~/.config/Suckless/x11
	$(DOAS) make -j4 install -C ~/.config/Suckless/x11/st
zsh: #WORK!
	$(PACMAN) zsh-syntax-highlighting
	cp -fvr ./home/.config/zsh ~/.config
	@zsh && compinit ; exit
	ln -f $(PWD)/home/.config/zsh/.zshrc ~/.zshrc
zram: #WORK!
	$(PACMAN) zramen zramen-openrc
	$(RC_ON) zramen
	$(DOAS) zramen make

push: evas clean #WORK!
	@git add --all
	@git add Makefile
	@read -p "Enter message: " MSG
	@git commit -am "$MSG"
	@git push
clean: #WORK!
	rm -rf home/.config/Suckless/dmenu/config.h
	rm -rf home/.config/Suckless/dmenu/*.o
	rm -rf home/.config/Suckless/dmenu/dmenu
	rm -rf home/.config/Suckless/dmenu/stest
	rm -rf home/.config/Suckless/dwm_new/config.h
	rm -rf home/.config/Suckless/dwm_new/dwm
	rm -rf home/.config/Suckless/dwm_new/dwm-msg
	rm -rf home/.config/Suckless/dwm_new/*.orig
	rm -rf home/.config/Suckless/dwm_new/*.rej
	rm -rf home/.config/Suckless/dwm_new/*.o
	rm -rf home/.config/Suckless/dwmblocks-async/build/*
	rm -rf home/.config/Suckless/dwmblocks_old/*.o
	rm -rf home/.config/Suckless/nsxiv/*.o
	rm -rf home/.config/Suckless/scroll/*.o
	rm -rf home/.config/Suckless/scroll/config.h
	rm -rf home/.config/Suckless/skippy-xd/skippy-xd
	rm -rf home/.config/Suckless/skippy-xd/*.o
	rm -rf home/.config/Suckless/st_new/config.h
	rm -rf home/.config/Suckless/st_new/st
	rm -rf home/.config/Suckless/st_new/*.o
	rm -rf home/.config/Suckless/tabbed/config.h
	rm -rf home/.config/Suckless/tabbed/*.o
emacs: # WORK!
	$(PACMAN) emacs libvterm hunspell hunspell-en_us hunspell-ru ttf-terminus-nerd
	$(PACMAN) libwebp libappindicator-gtk3 libnotify # Telega dependency
	$(PARU) tgs2png-git telegram-tdlib-git # Telega dependency
	rm -rf ~/.emacs.d
	cp -fvr ./home/.config/emacs ~/.config/emacs
	emacs --batch --eval "(require 'org)" --eval '(org-babel-tangle-file "~/.config/emacs/init.org")'
pkgbase: # WORK!
	$(PACMAN) $(BASEPKG)
pkgadd: # WORK!
	$(PACMAN) $(PACPKG)
paru: # WORK!
	$(PACMAN) base-devel
	rm -rf /tmp/paru
	git clone https://aur.archlinux.org/paru-bin.git /tmp/paru
	cd /tmp/paru && makepkg -si
aurpkg: #WORK!
	$(PARU) $(AURPKG)
kbd: #WORK!
	$(DOAS) cp -fvr ./sys/etc/X11/xorg.conf.d/00-keyboard.conf /etc/X11/xorg.conf.d/00-keyboard.conf
bluetooth: #WORK!
	$(PACMAN) bluez bluez-libs bluez-utils
	$(RC_ON) bluetoothd
pacman: #WORK
	$(PACMAN) artix-keyring chaotic-keyring archlinux-keyring
	$(DOAS) sed -i "s/^#Color/Color/" /etc/pacman.conf
        #sed -i "/#VerbosePkgLists/a ILoveCandy" /etc/pacman.conf
	$(DOAS) pacman-key --init artix
	$(DOAS) pacman-key --init archlinux
	$(DOAS) pacman-key --init chaotic
	$(DOAS) cp -fvr ./sys/etc/pacman.conf /etc/pacman.conf

doas: # WORK!
	$(PACMAN) opendoas
	sudo ln -s /usr/bin/doas /usr/bin/sudo
	echo -e "permit nopass :wheel\npermit nopass :plugdev as root cmd /usr/bin/brightnessctl" > /etc/doas.conf
	chown root:root /etc/doas.conf

# Games #
half-life:
	$(PARU) xash3d-fwgs-git hlextract gcfscape hlbsp-git studiomdl-git

tlp: # WORK!
	$(PACMAN) tlp tlp-openrc
	$(DOAS) cp -fvr ./sys/etc/tlp.conf /etc/tlp.conf
	$(RC_ON) tlp

gruvbox: # WORK!
	$(PACMAN) gsettings-desktop-schemas
	cp -fvr ./home/.config/gtk-2.0 ~/.config/gtk-2.0
	cp -fvr ./home/.config/gtk-3.0 ~/.config/gtk-3.0
	$(PARU) gruvbox-plus-icon-theme-git
	gsettings set org.gnome.desktop.interface gtk-theme "gruvbox-dark-gtk"
	gsettings set org.gnome.desktop.interface icon-theme "Gruvbox Plus Dark"
	gsettings set org.gnome.desktop.interface cursor-theme "Simp1e-Gruvbox-Dark"
#if [[ -e alacritty.org ]]; then echo True; else echo False; fi
# если есть файл alacritty.org тогда Правда, иначе Ложь
tty:
	if [[ -e ~/.config/alacritty/alacritty.org ]]; then cp -fvr ~/.config/alacritty ./home/.config; else echo Alacritty already stored; fi

save:
	@echo "use 'make push' instead"
evas: clean #WORK!
        # WALLPAPER #
	#cd ~/docs/pic/wal ; git add --all ; git commit -am "Walls" ; git push
        # ALACRITTY #
	if [[ -e ~/.config/alacritty/alacritty.org ]]; then cp -fvr ~/.config/alacritty ./home/.config ; cp -fvr ./home/.config/alacritty/alacritty.org ./home/.config/alacritty/README.org; else echo Alacritty already stored; fi
	cp -fvr ~/.config/gtk-2.0 ./home/.config/
	cp -fvr ~/.config/gtk-3.0 ./home/.config/
        # EMACS #
	if [[ -e ~/.config/emacs/init.org ]]; then cp -fvr ~/.config/emacs ./home/.config ; $(EVAL) '(org-babel-tangle-file "~/.config/emacs/init.org")' ; cp -fvr ./home/.config/emacs/init.org ./home/.config/emacs/README.org ; rm -rvf ./home/.config/emacs/var/*; else echo Emacs already stored; fi
        # SUCKLESS #
	cp -fvr ~/.config/Suckless ./home/.config/
	$(EVAL) '(org-babel-tangle-file "~/.config/Suckless/x11/dwm/dwm.org")'
	cp -fvr ~/.config/Suckless/x11/dwm/dwm.org ./home/.config/Suckless/x11/dwm/README.org
	$(EVAL) '(org-babel-tangle-file "~/.config/Suckless/x11/st/st.org")'
	cp -fvr ~/.config/Suckless/x11/st/st.org ./home/.config/Suckless/x11/st/README.org
        # BSPWM #
	if [[ -e ~/.config/bspwm/bspwm.org ]]; then $(EVAL) '(org-babel-tangle-file "~/.config/bspwm/bspwm.org")' ; cp -fvr ~/.config/bspwm ./home/.config ; cp -fvr ./home/.config/bspwm/bspwm.org ./home/.config/bspwm/README.org; else echo bspwm already stored; fi
        # POLYBAR #
	cp -fvr ~/.config/polybar ./home/.config/polybar
        # AWESOME #
	#cp -fvr ~/.config/awesome ./home/.config/
        # MPV #
	cp -fvr ~/.config/mpv ./home/.config/
        #DUNST
	cp -fvr ~/.config/dunst ./home/.config/
        # FOOT #
	if [[ -e ~/.config/foot/foot.ini ]]; then cp -fvr ~/.config/foot ./home/.config; else echo foot already stored; fi
        # QTILE #
	if [[ -e ~/.config/qtile/qtile.org ]]; then $(EVAL) '(org-babel-tangle-file "~/.config/qtile/qtile.org")' ; cp -fvr ~/.config/qtile ./home/.config ; cp -fvr ./home/.config/qtile/qtile.org ./home/.config/qtile/README.org; else echo Qtile already stored; fi
        # NEOVIM #
	if [[ -e ~/.config/nvim/nvim.org ]]; then $(EVAL) '(org-babel-tangle-file "~/.config/nvim/nvim.org")' ; cp -fvr ~/.config/nvim ./home/.config ; cp -fvr ./home/.config/nvim/nvim.org ./home/.config/nvim/README.org; else echo Neovim already stored; fi
	# ZATHURA #
	cp -fvr ~/.config/zathura ./home/.config/
	# X11 #
	cp -fvr ~/.config/X11 ./home/.config/
	# ZSH #
	if [[ -e ~/.config/zsh/zshrc.org ]]; then $(EVAL) '(org-babel-tangle-file "~/.config/zsh/zshrc.org")' ; cp -fvr ~/.config/zsh ./home/.config ; cp -fvr ./home/.config/zsh/zshrc.org ./home/.config/zsh/README.org; else echo zsh already stored; fi
	# LF #
	if [[ -e ~/.config/lf/lf.org ]]; then $(EVAL) '(org-babel-tangle-file "~/.config/lf/lf.org")' ; cp -fvr ~/.config/lf ./home/.config ; cp -fvr ./home/.config/lf/lf.org ./home/.config/lf/README.org; else echo lf already stored; fi
	# SDORFEHS #
	if [[ -e ~/.config/sdorfehs/sdorfehs.org ]]; then $(EVAL) '(org-babel-tangle-file "~/.config/sdorfehs/sdorfehs.org")' ; cp -fvr ~/.config/sdorfehs ./home/.config ; cp -fvr ./home/.config/sdorfehs/sdorfehs.org ./home/.config/sdorfehs/README.org; else echo Sdorfehs already stored; fi
	# SCRIPTS #
	cp -fvr ~/.local/bin/select_wm ./home/.local/bin/select_wm
	cp -fvr ~/.local/bin/dm_game ./home/.local/bin/dm_game
	# ETC #
	cp -fvr ~/.config/nsxiv ./home/.config/
	cp -fvr /etc/pacman.conf ./sys/etc/pacman.conf
	cp -fvr ~/.config/mimeapps.list ./home/.config/mimeapps.list
install: all
