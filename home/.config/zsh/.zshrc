autoload -U colors && colors
PS1="%B%{$fg[red]%}[%{$fg[yellow]%}%n%{$fg[green]%}@%{$fg[blue]%}%M %{$fg[magenta]%}%~%{$fg[red]%}]%{$reset_color%}$%b "

echo '\e[5 q'

#source $ZDOTDIR/zsh-vi-mode.zsh 2>/dev/null

bindkey ';5D' backward-word # ctrl+left
bindkey ';5C' forward-word #ctrl+right

typeset -U path
path=(~/bin ~/.local/bin $path[@])

HISTSIZE=10000
SAVEHIST=10000
HISTFILE=$ZDOTDIR/history

setopt inc_append_history
setopt share_history

eval "$(mcfly init zsh)"

export EDITOR="nvim"
export BROWSER="firefox"
export TERM="st"
export LANG=ru_RU.UTF-8

export XDG_DATA_HOME="$HOME/.local/share"
export XDG_CONFIG_HOME="$HOME/.config"
export XDG_STATE_HOME="$HOME/.local/state"
export XDG_CACHE_HOME="$HOME/docs/etc/.cache"
export XAUTHORITY="$XDG_RUNTIME_DIR"/Xauthority
export XINITRC="$XDG_CONFIG_HOME"/X11/xinitrc
export ZDOTDIR="$XDG_CONFIG_HOME/zsh"
export GTK2_RC_FILES="$XDG_CONFIG_HOME"/gtk-2.0/gtkrc
export WINEPREFIX="$HOME/docs/etc/.wine"
export BAT_THEME="gruvbox-dark"
export BAT_STYLE="plain"
export HISTFILE="$XDG_DATA_HOME"/bash_history #.bash_history
export CARGO_HOME="$XDG_DATA_HOME"/cargo
export GNUPGHOME="$XDG_DATA_HOME"/gnupg
export WAYFIRE_CONFIG_FILE="XDG_CONFIG_HOME"/wayfire/wayfire.ini
export _JAVA_OPTIONS=-Djava.util.prefs.userRoot="$XDG_CONFIG_HOME"/java
export XCURSOR_PATH=/usr/share/icons:$XDG_DATA_HOME/icons
export PEX_ROOT="$XDG_CACHE_HOME"/pex
export NVM_DIR="$XDG_DATA_HOME"/nvm
export GOPATH="$XDG_DATA_HOME"/go
export GOMODCACHE="$XDG_CACHE_HOME"/go/mod
export TEXMFVAR="$XDG_CACHE_HOME"/texlive/texmf-var
export GRAVEYARD="/tmp/graveyard-$USER"
export LF_BOOKMARK_PATH="$XDG_CONFIG_HOME"/lf/bm # lf bookmarks dir
source /usr/share/nvm/init-nvm.sh

alias cp="nocorrect rsync -r -h --info=progress2 --info=name0 --preallocate --archive --verbose --compress --partial --progress --recursive"
alias mv="nocorrect rsync -h --info=progress2 --info=name0 --preallocate --archive --verbose --compress --partial --progress --recursive --remove-source-files"
alias cd='z'
#alias mv='mv -iv'
#alias rm='rm -vI'
alias ls="ls --color=always"
alias la="ls -la"
alias rm="rip -i"
alias rmr="rip --unbury"
alias grep="rg --color always"
alias find="fd"
#alias cat="bat"
alias sudo="doas"
alias su="doas -s"
alias make="make -j4"
alias aria2c="aria2c -j4 -P"

alias dt='cd ~/docs/dotfiles'
alias mnt="lf /run/media/$USER"
alias cdd='cd ~/docs/docs'
alias cdw='cd ~/docs/dow'
alias cde='cd ~/docs/etc'
alias cdg='cd ~/docs/game'
alias cdm='cd ~/docs/mus'
alias cpd='cd ~/docs/pic'
alias cdv='cd ~/docs/vid'

alias dot="make -j4 $1 -C ~/docs/dots"
alias wget="wget --hsts-file="$XDG_CACHE_HOME"/wget-hsts"

alias manfzf="man -k . | fzf | awk '{print $1}' | xargs -r man"
alias mocp=mocp -O MOCDir="$XDG_CONFIG_HOME"/mocp
alias pacunused="sudo pacman -Qtdq"
alias fzf="fzf -x -e"
alias v='nvim'
alias emacs='emacsclient -c'
alias lf='lfub'
alias passgen="cat /dev/random | head --bytes 1 | shasum | head --bytes 10"
function cheat() {
  # Ask cheat.sh website for details about a Linux command.
  curl -m 10 "http://cheat.sh/${1}" 2>/dev/null || printf '%s\n' "[ERROR] Something broke"
}

alias translate='trans -I'

function man() {
  LESS_TERMCAP_mb=$'\e[01;31m' \
  LESS_TERMCAP_md=$'\e[01;31m' \
  LESS_TERMCAP_me=$'\e[0m' \
  LESS_TERMCAP_se=$'\e[0m' \
  LESS_TERMCAP_so=$'\e[45;93m' \
  LESS_TERMCAP_ue=$'\e[0m' \
  LESS_TERMCAP_us=$'\e[4;93m' \
  command man "$@" }

autoload -U compinit && compinit
zstyle ':completion:*' menu select
zmodload zsh/complist
compinit
_comp_options+=(globdots)		# Include hidden files.
source $ZDOTDIR/zsh-completions.zsh 2>/dev/null
source $ZDOTDIR/zsh-autosuggestions/zsh-autosuggestions.zsh
export ZSH_AUTOSUGGEST_HIGHLIGHT_STYLE="fg=#98971a,bold,underline"
export ZSH_AUTOSUGGEST_STRATEGY=(history completion)

fpath=($ZDOTDIR/zsh-completions/src $fpath)

# =============================================================================
#
# Utility functions for zoxide.
#

# pwd based on the value of _ZO_RESOLVE_SYMLINKS.
function __zoxide_pwd() {
    \builtin pwd -L
}

# cd + custom logic based on the value of _ZO_ECHO.
function __zoxide_cd() {
    # shellcheck disable=SC2164
    \builtin cd -- "$@"
}

# =============================================================================
#
# Hook configuration for zoxide.
#

# Hook to add new entries to the database.
function __zoxide_hook() {
    # shellcheck disable=SC2312
    \command zoxide add -- "$(__zoxide_pwd)"
}

# Initialize hook.
# shellcheck disable=SC2154
if [[ ${precmd_functions[(Ie)__zoxide_hook]:-} -eq 0 ]] && [[ ${chpwd_functions[(Ie)__zoxide_hook]:-} -eq 0 ]]; then
    chpwd_functions+=(__zoxide_hook)
fi

# =============================================================================
#
# When using zoxide with --no-cmd, alias these internal functions as desired.
#

__zoxide_z_prefix='z#'

# Jump to a directory using only keywords.
function __zoxide_z() {
    # shellcheck disable=SC2199
    if [[ "$#" -eq 0 ]]; then
        __zoxide_cd ~
    elif [[ "$#" -eq 1 ]] && { [[ -d "$1" ]] || [[ "$1" = '-' ]] || [[ "$1" =~ ^[-+][0-9]$ ]]; }; then
        __zoxide_cd "$1"
    elif [[ "$@[-1]" == "${__zoxide_z_prefix}"?* ]]; then
        # shellcheck disable=SC2124
        \builtin local result="${@[-1]}"
        __zoxide_cd "${result:${#__zoxide_z_prefix}}"
    else
        \builtin local result
        # shellcheck disable=SC2312
        result="$(\command zoxide query --exclude "$(__zoxide_pwd)" -- "$@")" &&
            __zoxide_cd "${result}"
    fi
}

# Jump to a directory using interactive search.
function __zoxide_zi() {
    \builtin local result
    result="$(\command zoxide query --interactive -- "$@")" && __zoxide_cd "${result}"
}

# Completions.
if [[ -o zle ]]; then
    function __zoxide_z_complete() {
        # Only show completions when the cursor is at the end of the line.
        # shellcheck disable=SC2154
        [[ "${#words[@]}" -eq "${CURRENT}" ]] || return 0

        if [[ "${#words[@]}" -eq 2 ]]; then
            _files -/
        elif [[ "${words[-1]}" == '' ]] && [[ "${words[-2]}" != "${__zoxide_z_prefix}"?* ]]; then
            \builtin local result
            # shellcheck disable=SC2086,SC2312
            if result="$(\command zoxide query --exclude "$(__zoxide_pwd)" --interactive -- ${words[2,-1]})"; then
                result="${__zoxide_z_prefix}${result}"
                # shellcheck disable=SC2296
                compadd -Q "${(q-)result}"
            fi
            \builtin printf '\e[5n'
        fi
        return 0
    }

    \builtin bindkey '\e[0n' 'reset-prompt'
    [[ "${+functions[compdef]}" -ne 0 ]] && \compdef __zoxide_z_complete __zoxide_z
fi

# =============================================================================
#
# Commands for zoxide. Disable these using --no-cmd.
#

\builtin alias z=__zoxide_z
\builtin alias zi=__zoxide_zi

# =============================================================================
#
# To initialize zoxide, add this to your configuration (usually ~/.zshrc):
#
eval "$(zoxide init zsh)"

bindkey ' ' magic-space
setopt correctall

# edit line
autoload edit-command-line; zle -N edit-command-line
bindkey '^e' edit-command-line
# Select wm
bindkey -s '^x' 'select_wm\n'
bindkey -s '^z' 'zi\n'

source /usr/share/zsh/plugins/zsh-syntax-highlighting/zsh-syntax-highlighting.zsh 2>/dev/null
