require('plugins')

-- Leader
vim.g.mapleader = ";"
-- File Manger
vim.keymap.set('n', '<leader>n', '<Cmd> lua MiniFiles.open()<CR>')

vim.api.nvim_set_keymap("n", "<leader>tn", ":$tabnew<CR>", { noremap = true })
vim.api.nvim_set_keymap("n", "<leader>tx", ":tabclose<CR>", { noremap = true })
vim.api.nvim_set_keymap("n", "<leader>to", ":tabonly<CR>", { noremap = true })
vim.api.nvim_set_keymap("n", "<leader>]", ":tabn<CR>", { noremap = true })
vim.api.nvim_set_keymap("n", "<leader>[", ":tabp<CR>", { noremap = true })
-- move current tab to previous position
vim.api.nvim_set_keymap("n", "<leader>tmp", ":-tabmove<CR>", { noremap = true })
-- move current tab to next position
vim.api.nvim_set_keymap("n", "<leader>tmn", ":+tabmove<CR>", { noremap = true })

-- Show tabline
--vim.wo.showtabline = 2

-- Show line numbers
vim.wo.number = true

vim.opts.list = true
listchars=space:⋅
--vim.opt.listchars:append "space:⋅"
--vim.opt.listchars:append "tab":>_

vim.o.background = "dark"
vim.o.termguicolors = true
vim.cmd.colorscheme 'gruvbox'

require("transparent").setup({
      groups = { -- table: default groups
	 'Normal', 'NormalNC', 'Comment', 'Constant', 'Special', 'Identifier',
	 'Statement', 'PreProc', 'Type', 'Underlined', 'Todo', 'String', 'Function',
	 'Conditional', 'Repeat', 'Operator', 'Structure', 'LineNr', 'NonText',
	 'SignColumn', 'CursorLineNr', 'EndOfBuffer',
      },
      extra_groups = {}, -- table: additional groups that should be cleared
      exclude_groups = {}, -- table: groups you don't want to clear
			    })
vim.g.transparent_enabled = true

vim.loader.enable()

vim.g.shebang_commands = { sh = '/bin/bash' }

local theme = {
   fill = 'TabLineFill',
   -- Also you can do this: fill = { fg='#f2e9de', bg='#907aa9', style='italic' }
   head = 'TabLine',
   current_tab = 'TabLineSel',
   tab = 'TabLine',
   win = 'TabLine',
   tail = 'TabLine',
}

require('tabby.tabline').set(function(line)
      return {
	 {
	    { '  ', hl = theme.head },
	    line.sep('', theme.head, theme.fill),
	 },
	 line.tabs().foreach(function(tab)
	       local hl = tab.is_current() and theme.current_tab or theme.tab
	       return {
		  line.sep('', hl, theme.fill),
		  tab.is_current() and '' or '󰆣',
		  tab.number(),
		  tab.name(),
		  tab.close_btn(''),
		  line.sep('', hl, theme.fill),
		  hl = hl,
		  margin = ' ',
	       }
			    end),
	 line.spacer(),
	 line.wins_in_tab(line.api.get_current_tab()).foreach(function(win)
	       return {
		  line.sep('', theme.win, theme.fill),
		  win.is_current() and '' or '',
		  win.buf_name(),
		  line.sep('', theme.win, theme.fill),
		  hl = theme.win,
		  margin = ' ',
	       }
							     end),
	 {
	    line.sep('', theme.tail, theme.fill),
	    { '  ', hl = theme.tail },
	 },
	 hl = theme.fill,
      }
			    end)

require('nvim-treesitter.configs').setup({
      highlight = {
	 enable = true,
      },
					})

require('mini.completion').setup() -- Mini completion
require('mini.cursorword').setup() -- Mini word highlight
require('mini.files').setup()
require('mini.move').setup()
require('mini.indentscope').setup()
require('mini.bracketed').setup()
require('mini.pairs').setup()
local hipatterns = require('mini.hipatterns')
hipatterns.setup({
      highlighters = {
	 -- Highlight standalone 'FIXME', 'HACK', 'TODO', 'NOTE'
	 fixme = { pattern = '%f[%w]()FIXME()%f[%W]', group = 'MiniHipatternsFixme' },
	 hack  = { pattern = '%f[%w]()HACK()%f[%W]',  group = 'MiniHipatternsHack'  },
	 todo  = { pattern = '%f[%w]()TODO()%f[%W]',  group = 'MiniHipatternsTodo'  },
	 note  = { pattern = '%f[%w]()NOTE()%f[%W]',  group = 'MiniHipatternsNote'  },

	 -- Highlight hex color strings (`#rrggbb`) using that color
	 hex_color = hipatterns.gen_highlighter.hex_color(),
      },
})

require('lualine').setup {
   options = {
      icons_enabled = true,
      theme = 'gruvbox-material',
      component_separators = { left = '', right = ''},
      section_separators = { left = '', right = ''},
      disabled_filetypes = {
	 statusline = {},
	 winbar = {},
      },
      ignore_focus = {},
      always_divide_middle = true,
      globalstatus = false,
      refresh = {
	 statusline = 1000,
	 tabline = 1000,
	 winbar = 1000,
      }
   },
   sections = {
      lualine_a = {'mode'},
      lualine_c = {'filename'},
      lualine_x = {'filetype'},
      lualine_y = {'progress'},
      lualine_z = {'location'}
   },
   inactive_sections = {
      lualine_a = {},
      lualine_b = {},
      lualine_c = {'filename'},
      lualine_x = {'location'},
      lualine_y = {},
      lualine_z = {}
   },
   tabline = {},
   winbar = {},
   inactive_winbar = {},
   extensions = {}
			 }
