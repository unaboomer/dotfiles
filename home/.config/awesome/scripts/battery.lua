local awful = require("awful")

local update_interval = 60
local get_capacity = [[
sh -c "cat /sys/class/power_supply/BAT*/capacity"
]]
local get_status = [[
sh -c "cat /sys/class/power_supply/BAT*/uevent | head -n 3 | tail -n 1"
]]

awful.widget.watch(get_capacity, update_interval, function(widget,stdout)
	local value = string.gsub(stdout, '^%s*(.-)%s*$','%1')
	awesome.emit_signal("bat::value", value)
end)

awful.widget.watch(get_status, update_interval, function(widget,stdout)
	local value = string.gsub(stdout, '^%s*(.-)%s*$','%1')
	awesome.emit_signal("bat::state", value)
end)
