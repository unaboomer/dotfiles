//Modify this file to change what commands output to your statusbar, and recompile using the make command.
static const Block blocks[] = {
	/*Icon*/  /*Command*/	       /*Update Interval*/	    /*Update Signal*/
	{"",	  "db_battery",	       5,	                          3},
	{"",	  "db_layout",         1,                                10},
	{"",      "db_sound",          1,                                10},
	{"",      "db_wttr",           1800,                              5},
//	{"",	  "date +%H:%M",       1,	                          8},
	{"",	  "db_date",	       1,				  8},
};

//Sets delimiter between status commands. NULL character ('\0') means no delimiter.
static char *delim = "|";

// Have dwmblocks automatically recompile and run when you edit this file in
// vim with the following line in your vimrc/init.vim:

// autocmd BufWritePost ~/.local/src/dwmblocks/config.h !cd ~/.local/src/dwmblocks/; sudo make install && { killall -q dwmblocks;setsid dwmblocks & }
