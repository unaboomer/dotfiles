#+title: Suckless Dynamic Window Manager config file
#+startup: overview
#+property: header-arg:c

#+CAPTION: Suckless DWM Screenshot
#+ATTR_HTML: :alt DWM Screnshot :title DWM Scrot :align left
[[https://gitlab.com/unaboomer/dotfiles/-/raw/main/home/.config/Suckless/x11/dwm/dwm.png]]

* Start script
#+begin_src shell-script :tangle ./Scripts/startdwm
#!/bin/bash
pipewire 2>&1 >/dev/null &
pipewire-pulse &
wireplumber 2>&1 >/dev/null &
picom --vsync -b &
nitrogen --set-scaled docs/pic/wal/gruv.png &
emacs --daemon &
dwmblocks 2>&1 >/dev/null &
skippy-xd --start-daemon &
setxkbmap us,ru winkeys grp:alt_shift_toggle &
mocp -S &
warpd &
dwm
#+end_src
* Xinitrc
#+begin_src shell-script :tangle ~/.config/X11/xinitrc
picom --shadow --shadow-radius 12 --shadow-opacity 0.75 --fading --fade-in-step 0.028 --fade-out-step 0.03 --vsync --use-damage --animations -b
export ZDOTDIR="$HOME/.config/zsh" ;
killall pipewire ; killall wireplumber ;
pipewire &
wireplumber &
startdwm
#+end_src
* Constants
#+begin_src c :tangle ./config.def.h
#define TERMINAL "st"
#define TERMCLASS "St"
#define BROWSER "firefox"
#define SESSION_FILE "/tmp/dwm-session"
#+end_src

* Appearance
** Settings
#+begin_src c :tangle ./config.def.h
static const unsigned int borderpx  = 4;        /* border pixel of windows */
static const Gap default_gap        = {.isgap = 1, .realgap = 10, .gappx = 0};
static const unsigned int snap      = 32;       /* snap pixel */
static const int swallowfloating    = 1;        /* 1 means swallow floating windows by default */
static const int scalepreview       = 5;        /* preview scaling (display w and h / scalepreview) */
static const int previewbar         = 1;        /* show the bar in the preview window */
static const int showbar            = 1;        /* 0 means no bar */
static const int topbar             = 1;        /* 0 means bottom bar */
#define ICONSIZE 22   /* icon size */
//#define ICONSIZE bh       /* make icon size equals to bar height */
//#define ICONSIZE (bh - 4) /* or adaptively preserve 2 pixels each side */
#define ICONSPACING 5 /* space between icon and title */
static const int user_bh            = 0;        /* 0 means that dwm will calculate bar height, >= 1 means dwm will user_bh as bar height */
static const int viewonrulestag     = 1; 	/* 1 means when open applications view will move to tags defined in rules*/
static const unsigned int systraypinning = 0;   /* 0: sloppy systray follows selected monitor, >0: pin systray to monitor X */
static const unsigned int systrayonleft = 0;    /* 0: systray in the right corner, >0: systray on left of status text */
static const unsigned int systrayspacing = 2;   /* systray spacing */
static const int systraypinningfailfirst = 1;   /* 1: if pinning fails, display systray on the first monitor, False: display systray on the last monitor*/
static const int showsystray        = 1;        /* 0 means no systray */
static const char *fonts[]          = { "Terminess Nerd Font:size=12:antialias=true:autohint=true" };
static const char dmenufont[]       = "Terminess Nerd Font:size=12:antialias=true:autohint=true";
static const XPoint stickyicon[]    = { {0,0}, {4,0}, {4,8}, {2,6}, {0,8}, {0,0} }; /* represents the icon as an
array of vertices */
static const XPoint stickyiconbb    = {4,8};   /* defines the bottom right corner of the polygon's bounding box (speeds up scaling) */
#+end_src

** Colors
#+begin_src c :tangle ./config.def.h
static const char normbgcolor[]         = "#282828"; /* Tags background colors */
static const char normbordercolor[]     = "#444444";
static const char normfgcolor[]         = "#ebdbb2"; /* Text color */
static const char selfgcolor[]          = "#eeeeee";
static const char selbordercolor[]      = "#98971a"; /* Border color */
static const char selbgcolor[]          = "#504945"; /* Bar color */

/* solarized colors http://ethanschoonover.com/solarized */
static const char s_base03[]            = "#002b36";
static const char s_base02[]            = "#073642";
static const char s_base01[]            = "#586e75";
static const char s_base00[]            = "#657b83";
static const char s_base0[]             = "#839496";
static const char s_base1[]             = "#93a1a1";
static const char s_base2[]             = "#eee8d5";
static const char s_base3[]             = "#fdf6e3";

/* Solarized white */
static const char sol_normbgcolor[]     = "#fdf6e3"; /* Tags background colors */
static const char sol_normbordercolor[] = "#fdf6e3";
static const char sol_normfgcolor[]     = "#839496"; /* Text color */
static const char sol_selfgcolor[]      = "#fdf6e3";
static const char sol_selbordercolor[]  = "#586e75"; /* Border color */
static const char sol_selbgcolor[]      = "#586e75"; /* Bar color */

/* Everforest */
static const char evf_normbgcolor[]     = "#2f383e"; /* Tags background colors */
static const char evf_normbordercolor[] = "#868d80";
static const char evf_normfgcolor[]     = "#d8caac"; /* Text color */
static const char evf_selfgcolor[]      = "#2f383e";
static const char evf_selbordercolor[]  = "#a7c080"; /* Border color */
static const char evf_selbgcolor[]      = "#a7c080"; /* Bar color */

/* Gentoo */
static const char gen_normbgcolor[]     = "#0d0913"; /* Tags background colors */
static const char gen_normbordercolor[] = "#95879e";
static const char gen_normfgcolor[]     = "#d5c2e3"; /* Text color */
static const char gen_selfgcolor[]      = "#d5c2e3";
static const char gen_selbordercolor[]  = "#d5c2e3"; /* Border color */
static const char gen_selbgcolor[]      = "#983CCE"; /* Bar color */

static const char *colors[][3]      = {
    /*fg           bg           border          */
    //{ s_base0,     s_base03, s_base2 },      /* SchemeNorm dark */
    //{ s_base0,     s_base02, s_base2 },      /* SchemeSel dark */
    //{ s_base00,    s_base3, s_base02 },     /* SchemeNorm light */
    //{ s_base00,    s_base2, s_base02},      /* SchemeSel light */
    { normfgcolor, normbgcolor, normbordercolor }, /* SchemeNorm orig */
    { selfgcolor,  selbgcolor,  selbordercolor  }, /* SchemeSel orig */

    { gen_normfgcolor, gen_normbgcolor, gen_normbordercolor }, /* Gentoo */
    { gen_selfgcolor,  gen_selbgcolor,  gen_selbordercolor  }, /* Gentoo */

    { evf_normfgcolor, evf_normbgcolor, evf_normbordercolor }, /* Everforest */
    { evf_selfgcolor,  evf_selbgcolor,  evf_selbordercolor  }, /* Everforest */

    { sol_normfgcolor, sol_normbgcolor, sol_normbordercolor }, /* Solarized light */
    { sol_selfgcolor,  sol_selbgcolor,  sol_selbordercolor  }, /* Solarized dark */
};
#+end_src

* Autostart
#+begin_src c :tangle ./config.def.h
static const char *const autostart[] = {
    "killall pipewire &", NULL,
    "pipewire &", NULL,
    "wireplumber &", NULL,
    "dwmblocks 2>&1/dev/null", NULL,
    "skippy-xd --start-daemon", NULL,
    "emacs --daemon", NULL,
    "mocp -S", NULL,
    "lf -server", NULL,
    "udiskie -a -n -T --file-manager lf", NULL,
    "setxkbmap us,ru winkeys grp:alt_shift_toggle &", NULL,
    "dunst &", NULL,
    NULL /* terminate */
};
#+end_src

* Tagging
#+begin_src c :tangle ./config.def.h

static const char *tags[] = { "1", "2", "3", "4", "5", "6", "7", "8", "9" };

static const unsigned int ulinepad  = 5;    /* horizontal padding between the underline and tag */
static const unsigned int ulinestroke   = 2;    /* thickness / height of the underline */
static const unsigned int ulinevoffset  = 0;    /* how far above the bottom of the bar the line should appear */
static const int ulineall       = 0;    /* 1 to show underline on all tags, 0 for just the active ones */

static const char *tagsel[][2] = {
    { "#ebdbb2", "#cc241d" },
    { "#ebdbb2", "#d65d0e" },
    { "#ebdbb2", "#d79921" },
    { "#ebdbb2", "#98971a" },
    { "#ebddb2", "#458588" },
    { "#ebdbb2", "#b16286" },
    { "#ebdbb2", "#d3869b" },
    { "#1d2021", "#ebdbb2" },
    { "#ebdbb2", "#1d2021" },
};

#+end_src

* Window Rules
#+begin_src c :tangle ./config.def.h
static const Rule rules[] = {
    /* xprop(1):
     *  xprop | sed -n '5p;15p'
     *  WM_CLASS(STRING) = instance, class
     *  WM_NAME(STRING) = title
     */
	/* class     	        instance	title			tags mask    isfloating  float x,y,w,h	   isterminal    noswallow    monitor 	floatborderpx	*/
	{ "Firefox", 	        NULL,		NULL,            	1 << 8,      0,          50,50,500,500,	   0,           -1,           -1,	5,	},
   	{ "St", 	        NULL,		NULL,            	0,           0,          50,50,500,500,	   1,            0,           -1, 	5,	},
	{ "TelegramDesktop",	NULL,		"Просмотр медиа",	0,	     1,		 50,50,1280,720,   0,		 1,	      -1,	5,	},
	{ NULL,      	        NULL,		"Event Tester",  	0,           0,          50,50,500,500,	   0,            1,           -1, 	5,	}, /* xev */
};
#+end_Src

* Layout(s)
#+begin_src c :tangle ./config.def.h
static const float mfact     = 0.60; /* factor of master area size [0.05..0.95] */
static const int nmaster     = 1;    /* number of clients in master area */
static const int resizehints = 1;    /* 1 means respect size hints in tiled resizals */
static const int lockfullscreen = 1; /* 1 will force focus on the fullscreen window */
static const int attachdirection = 0;    /* 0 default, 1 above, 2 aside, 3 below, 4 bottom, 5 top */

/* mouse scroll resize */
static const int scrollsensetivity = 30; /* 1 means resize window by 1 pixel for each scroll event */

#include "fibonacci.c"
//#include "grid.c"
#include "gaplessgrid.c"
static const Layout layouts[] = {
	/* symbol     arrange function */
	{ "[]=",      tile },    /* first entry is default */
	{ "><>",      NULL },    /* no layout function means floating behavior */
	{ "[M]",      monocle },
	{ "[###]",    gaplessgrid },
	//{ "[HHH]",    grid },
	{ "[@]",      spiral },
	{ "[\\]",     dwindle },
	{ NULL,       NULL },
};
#+end_src

* key definitions
#+begin_src c :tangle ./config.def.h
#define MODKEY Mod4Mask // Window key
#define ALTKEY Mod1Mask // Alt key

#define TAGKEYS(KEY,TAG) \
    { MODKEY,                       KEY,      view,           {.ui = 1 << TAG} }, \
    { MODKEY|ControlMask,           KEY,      toggleview,     {.ui = 1 << TAG} }, \
    { MODKEY|ShiftMask,             KEY,      tag,            {.ui = 1 << TAG} }, \
    { ALTKEY,           	    KEY,      previewtag,     {.ui = TAG } },
#+end_src

* Commands
#+begin_src c :tangle ./config.def.h
/* helper for spawning shell commands in the pre dwm-5.0 fashion */
#define SHCMD(cmd) { .v = (const char*[]){ "/bin/bash", "-c", cmd, NULL } }
/* Pipes "|" dont work in spawn */
static const char *dmenucmd[] = { "dwmenu", topbar ? NULL : "-b", NULL };
static const char *dmenuhub[] = { "dm_hub", NULL };
static const char *dwmexit[]  = { "dwm_exit", NULL };
static const char *browser[]  = { "firefox", NULL };
static const char *termcmd[]  = { "st", NULL };
static const char *scrfull[]  = { "dwmshot", "-full", NULL };
static const char *scrpart[]  = { "dwmshot", "-part", NULL };
static const char *music[]  = {TERMINAL, "mocp", "-y", "-O", "SoundDriver=ALSA:JACK:OSS", "-O", "SyncPlaylist=yes", "-O", "Theme=transparent-background", "-O", "MusicDir=/home/andrew/docs/mus", NULL };
static const char *mixer[]  = {TERMINAL, "alsamixer", "--card=0", "--view=all", NULL };
static const char *emacs[]  = { "emacsclient", "-c", NULL };
static const char *brightup[]  = { "brightctl", "up", NULL };
static const char *brightdown[]  = { "brightctl", "down", NULL };
static const char *volup[]  = { "volctl", "up", NULL };
static const char *voldown[]  = { "volctl", "down", NULL };
static const char *volmute[]  = { "volctl", "mute", NULL };
static const char *skippy[]  = { "skippy-xd", "--config", ".config/Suckless/skippy-xd/skippy-xd.rc", NULL };
static const char *bluezmenu[]  = { "dm_bluetooth", NULL };
static const char *record[]  = { "dm_record", NULL };
static const char *torrent[]  = {TERMINAL, "tremc", NULL };
static const char *randwal[]  = { "nitrogen", "--set-scaled", "--random", NULL };
static const char *yt[]  = { "ytfzf", "-D", NULL };
static const char *cheat[]  = { "hotkeys", NULL };
#+end_src

* Hotkeys
Use getkeycode script to know key name
Press Fn+Super+Ecs to press F-keys without holding Fn

Unused:
{MODKEY|ControlMask|ShiftMask, 111, moveresizeedge,  {.v = "T"} }, // Dont delete
{MODKEY|ControlMask|ShiftMask, 116, moveresizeedge,  {.v = "B"} },
{MODKEY|ControlMask|ShiftMask, 113, moveresizeedge,  {.v = "L"} },
{MODKEY|ControlMask|ShiftMask, 114, moveresizeedge,  {.v = "R"} },
{ MODKEY,		       115,  quit,	     {0} }, // End

#+begin_src c :tangle ./config.def.h
#include <X11/XF86keysym.h>
#include "nextprevtag.c"

static const Key keys[] = {
/* modifier                    key  function           argument                                 //{key}    //{icon}//{"description"} */
{MODKEY,                       40,  spawn,             {.v = dmenucmd}},                        //d        //#//"Launch specific dmenu script"
{MODKEY|ShiftMask,             40,  spawn,             {.v = dmenuhub}},                        //d              //#//"Launch dmenu"
{MODKEY,                       36,  spawn,             {.v = termcmd}},                         //Return         //#//"Launch terminal"
{MODKEY,                       49,  spawn,             {.v = termcmd}},                         //`              //#//"Launch terminal"
{MODKEY,                       39,  spawn,             {.v = scrfull}},                         //s      //#//"Shot full screen"
{MODKEY|ControlMask,           39,  spawn,             {.v = scrpart}},                         //s              //#//"Shot part of screen"
{MODKEY,	               29,  togglesticky,      {0}},                                    //y      //#//"Toggle stitky"
{MODKEY,                      107,  spawn,             {.v = scrfull}},                         //Print  //#//"Shot full screen"
{MODKEY|ControlMask,          107,  spawn,             {.v = scrpart}},                         //Print         //#//"Shot part of screen"
{0,			       76,  spawn,	       {.v = cheat}},                           //F10           //#//"Show cheat sheet"
{0, 			       73,  spawn,	       {.v = yt}},                              //F7             //#//"Search youtube"
//static const char *emacs[]  = { "emacsclient", "-c", NULL };
{MODKEY,		       26,  spawn,	       SHCMD("emacsclient -c || emacs")},       //e              //#//"Open emacs"
{MODKEY,		       58,  spawn,	       {.v = music}},                           //m        //#//"Launch mocp"
{MODKEY|ShiftMask,	       58,  spawn,	       {.v = mixer}},                           //m             //#//"Launch alsamixer"
{0,			       74,  spawn,	       {.v = bluezmenu}},                       //F8             //#//"Launch bluetooh menu"
{MODKEY,		       27,  spawn,	       {.v = record}},                          //r             //#//"Record screen"
{0,			       75,  spawn,	       {.v = torrent}},                         //F9            //#//"Launch torrent"
{0,                            67,  spawn,             {.v = volmute}},                         //F1            //#//"Mute volume"
{0,                            69,  spawn,             {.v = volup}},                           //F3            //#//"Volume +5"
{0,                            68,  spawn,             {.v = voldown}},                         //F2             //#//"Volume -5"
{MODKEY,		       25,  spawn,	       {.v = browser}},                         //w        //#//"Launch browser"
{MODKEY|ShiftMask, 	       25,  spawn,             {.v = randwal}},                         //w             //#//"Random wallpaper"
{0,			       96,  spawn,             {.v = brightup}},                        //F12           //#//"Brightness +5"
{0, 			       95,  spawn,             {.v = brightdown}},                      //F11      //#//"Brightness -5"
{MODKEY|ShiftMask,             27,  quit,              {1}},                                    //r      //#//"Restart DWM"
{MODKEY|ControlMask,           27,  resetlayout,       {0}},                                    //Control+r      //#//"Reset layout"
{MODKEY,                       56,  togglebar,         {0}},                                    //b        //#//"Toggle bar"
{MODKEY|ShiftMask,             55,  toggleborder,      {0}},                                    //v              //#//"Toggle border"
{MODKEY,                       45,  focusstack,        {.i = +1}},                              //k              //#//"Focus window"
{MODKEY,                       44,  focusstack,        {.i = -1}},                              //j              //#//"Focus window"
{MODKEY,                       31,  incnmaster,        {.i = +1}},                              //i        //#//"Increase amount of master windows"
{MODKEY|ShiftMask,             31,  incnmaster,        {.i = -1}},                              //i         //#//"Descrease amount of master windows"
{MODKEY,                       43,  setmfact,          {.f = -0.05}},                           //h         //#//"Resize window on left"
{MODKEY,                       46,  setmfact,          {.f = +0.05}},                           //l   //#//"Resize window on right"
{MODKEY|ShiftMask,             36,  zoom,              {0}},                                    //Return    //#//"Swap windows"
{MODKEY,                       23,  view,              {0}},                                    //Tab       //#//"Return to previous tag"
{ALTKEY,                       23,  spawn,             {.v = skippy}},                          //Tab //#//"Launch Skippy-xd"
{MODKEY|ShiftMask,             24,  killclient,        {0} },                                   //q         //#//"Kill window"
{MODKEY,                       28,  setlayout,         {.v = &layouts[0]}},                     //t   //#//"unknow"
{MODKEY|ShiftMask,             28,  togglealwaysontop, {0}},                                    //t   //#//"Toggle always on top"
{MODKEY|ShiftMask,             33,  schemeToggle,      {0}},                                    //p         //#//"Toggle scheme"
{MODKEY,                       33,  schemeCycle,       {0}},                                    //p         //#//"Cycle scheme"
{MODKEY,                      116,  moveresize,        {.v = "0x 25y 0w 0h"}},                  //Down      //#//"Move floating window down"
{MODKEY,                      111,  moveresize,        {.v = "0x -25y 0w 0h"}},                 //Up        //#//"Move floating window up"
{MODKEY,                      114,  moveresize,        {.v = "25x 0y 0w 0h"}},                  //Right     //#//"Move floating window right"
{MODKEY,                      113,  moveresize,        {.v = "-25x 0y 0w 0h"}},                 //Left//#//"Move floating window left"
{MODKEY|ShiftMask,            116,  moveresize,        {.v = "0x 0y 0w 25h"}},                  //Down//#//"Resize window down"
{MODKEY|ShiftMask,            111,  moveresize,        {.v = "0x 0y 0w -25h"}},                 //Up  //#//"Resize window up"
{MODKEY|ShiftMask,            114,  moveresize,        {.v = "0x 0y 25w 0h"}},                  //Right //#//"Resize window right"
{MODKEY|ShiftMask,            113,  moveresize,        {.v = "0x 0y -25w 0h"}},                 //Left//#//"Resize window left"
{MODKEY|ControlMask,          111,  moveresizeedge,    {.v = "t"}},                             //Up  //#//"Move window top of screen"
{MODKEY|ControlMask,          116,  moveresizeedge,    {.v = "b"}},                             //Down//#//"Move window bottom of screen"
{MODKEY|ControlMask,          113,  moveresizeedge,    {.v = "l"}},                             //Left//#//"Move window left top of screen"
{MODKEY|ControlMask,          114,  moveresizeedge,    {.v = "r"}},                             //Right //#//"Move windw right top of screen"
{MODKEY|ShiftMask,             42,  setgaps,           {.i = -1}},                              //g           //#//"gaps -1"
{MODKEY,                       42,  setgaps,           {.i = +1}},                              //g   //#//"gaps +1"
{MODKEY|ControlMask,           42,  setgaps,           {.i = GAP_TOGGLE}},                      //g   //#//"Toggle gaps"
{MODKEY|ControlMask,           21,  setgaps,           {.i = GAP_RESET}},                       //=           //#//"Reset gaps"
{MODKEY,                       52,  cyclelayout,       {.i = -1}},                              //z          //#//"Cycle layout -1"
{MODKEY,                       53,  cyclelayout,       {.i = +1}},                              //x          //#//"Cycle layout +1"
{MODKEY,                       65,  setlayout,         {0} },                                   //space//#//"Floating layout"
{MODKEY|ShiftMask,             65,  togglefloating,    {0}},                                    //space      //#//"unknown"
{MODKEY,                       41,  togglefullscr,     {0}},                                    //f          //#//"Toggle fullscreen"
{MODKEY,                       19,  view,              {.ui = ~0}},                             //0    //#//"Show all tags"
{MODKEY|ShiftMask,             19,  tag,               {.ui = ~0}},                             //0    //#//"Tag"
TAGKEYS(                       10,                     0)                                       //1         //#//"Go to 1 tag"
TAGKEYS(                       11,                     1)                                       //2         //#//"Go to 2 tag"
TAGKEYS(                       12,                     2)                                       //3         //#//"Go to 3 tag"
TAGKEYS(                       13,                     3)                                       //4         //#//"Go to 4 tag"
TAGKEYS(                       14,                     4)                                       //5         //#//"Go to 5 tag"
TAGKEYS(                       15,                     5)                                       //6         //#//"Go to 6 tag"
TAGKEYS(                       16,                     6)                                       //7         //#//"Go to 7 tag"
TAGKEYS(                       17,                     7)                                       //8         //#//"Go to 8 tag"
TAGKEYS(                       18,                     8)                                       //9         //#//"Go to 9 tag"
{MODKEY|ShiftMask,            115, quit,               {1}},                                    //End       //#//"Restart DWM"
{0,                           115, spawn,              {.v = dwmexit}},                         //End            //#//"dwm exit menu"

};
#+end_src

* Button definitions
#+begin_src c :tangle ./config.def.h
/* resizemousescroll direction argument list */
static const int scrollargs[][2] = {
    /* width change         height change */
    { +scrollsensetivity,   0 },
    { -scrollsensetivity,   0 },
    { 0,                    +scrollsensetivity },
    { 0,                    -scrollsensetivity },
};

/* click can be ClkTagBar, ClkLtSymbol, ClkStatusText, ClkWinTitle, ClkClientWin, or ClkRootWin */
static const Button buttons[] = {
    /* click                event mask        button          function        argument */
    { ClkLtSymbol,          0,                Button1,        cyclelayout,    {.i = +1} },
    { ClkLtSymbol,          0,                Button3,        cyclelayout,    {.i = -1} },
    { ClkLtSymbol,          0,                Button4,        cyclelayout,    {.i = +1} },
    { ClkLtSymbol,          0,                Button5,        cyclelayout,    {.i = -1} },

    { ClkTagBar,            MODKEY,           Button1,        tag,            {0} },
    { ClkTagBar,            MODKEY,           Button3,        toggletag,      {0} },
    { ClkTagBar,            0,                Button1,        view,           {0} },
    { ClkTagBar,            0,                Button3,        toggleview,     {0} },
    { ClkTagBar,            MODKEY,           Button1,        tag,            {0} },
    { ClkTagBar,            MODKEY,           Button3,        toggletag,      {0} },
    { ClkTagBar,            0,        	      Button4,        view_adjacent,  { .i = +1 } },
    { ClkTagBar,            0,        	      Button5,        view_adjacent,  { .i = -1 } },

    /* Two fingers (Left, right and Up and down) */
    { ClkClientWin,         MODKEY,           Button4,        resizemousescroll, {.v = &scrollargs[0]} },
    { ClkClientWin,         MODKEY,           Button5,        resizemousescroll, {.v = &scrollargs[1]} },
    { ClkClientWin,         MODKEY,           Button6,        resizemousescroll, {.v = &scrollargs[2]} },
    { ClkClientWin,         MODKEY,           Button7,        resizemousescroll, {.v = &scrollargs[3]} },

    { ClkWinTitle,          0,                Button2,        zoom,           {0} },
    { ClkStatusText,        0,                Button1,        sigdwmblocks,   {.i = 1} },
    { ClkStatusText,        0,                Button2,        sigdwmblocks,   {.i = 2} },
    { ClkStatusText,        0,                Button3,        sigdwmblocks,   {.i = 3} },

    { ClkClientWin,         MODKEY,           Button1,        movemouse,      {0} },
    { ClkClientWin,         MODKEY,           Button2,        togglefloating, {0} },
    { ClkClientWin,         MODKEY,           Button3,        resizemouse,    {0} },
};
#+end_src

* IPC
#+begin_src c :tangle ./config.def.h
/* https://github.com/mihirlad55/dwm-ipc/wiki/ */
// Example: "dwm-msg run_command cyclelayout"
// dwm-msg run_command setgaps 5
//dwm-msg run_command quit 0 - Exit DWM
//dwm-msg run_command quit 1 - Restart dwm
static const char *ipcsockpath = "/tmp/dwm.sock";
static IPCCommand ipccommands[] = {
  IPCCOMMAND(  view,                1,      {ARG_TYPE_UINT}   ),
  IPCCOMMAND(  toggleview,          1,      {ARG_TYPE_UINT}   ),
  IPCCOMMAND(  tag,                 1,      {ARG_TYPE_UINT}   ),
  IPCCOMMAND(  toggletag,           1,      {ARG_TYPE_UINT}   ),
  IPCCOMMAND(  tagmon,              1,      {ARG_TYPE_UINT}   ),
  IPCCOMMAND(  focusmon,            1,      {ARG_TYPE_SINT}   ),
  IPCCOMMAND(  focusstack,          1,      {ARG_TYPE_SINT}   ),
  IPCCOMMAND(  zoom,                1,      {ARG_TYPE_NONE}   ),
  IPCCOMMAND(  incnmaster,          1,      {ARG_TYPE_SINT}   ),
  IPCCOMMAND(  killclient,          1,      {ARG_TYPE_SINT}   ),
  IPCCOMMAND(  togglefloating,      1,      {ARG_TYPE_NONE}   ),
  IPCCOMMAND(  setmfact,            1,      {ARG_TYPE_FLOAT}  ),
  IPCCOMMAND(  setlayoutsafe,       1,      {ARG_TYPE_PTR}    ),
  IPCCOMMAND(  quit,                1,      {ARG_TYPE_UINT}   ),
  IPCCOMMAND(  cyclelayout,         1,      {ARG_TYPE_NONE}   ),
  IPCCOMMAND(  view_adjacent,       1,	    {ARG_TYPE_NONE}   ),
  IPCCOMMAND(  setgaps,		    1,      {ARG_TYPE_SINT}   ),
  IPCCOMMAND(  schemeCycle,	    1,	    {ARG_TYPE_NONE}   ),
  IPCCOMMAND(  togglebar,	    1,	    {ARG_TYPE_NONE}   ),
  IPCCOMMAND(  toggleborder,	    1,	    {ARG_TYPE_NONE}   )
};
#+end_src
