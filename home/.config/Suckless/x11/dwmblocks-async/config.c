#include "config.h"

#include "block.h"
#include "util.h"

Block blocks[] = {
    /*Command*/		/*Update interval*/	/*Update Signal*/
    {"db_battery",    	5,  			3 },
    {"db_layout",   	1,    			9 },
    {"db_sound",    	1,    			10 },
    {"db_light",	1,			6 },
    {"db_wttr",  	1800,  			5 },
    {"db_date", 	1,    			8 },
};

const unsigned short blockCount = LEN(blocks);
