;;; Compiled snippets and support files for `emacs-lisp-mode'
;;; Snippet definitions:
;;;
(yas-define-snippets 'emacs-lisp-mode
		     '(("@use" "(use-package $1\n  :ensure t\n   $0)\n" "use-package" nil nil nil "/home/andrew/.config/emacs/etc/yasnippet/snippets/emacs-lisp-mode/use-package" nil nil)))


;;; Do not edit! File generated at Sat Nov 18 14:14:03 2023
