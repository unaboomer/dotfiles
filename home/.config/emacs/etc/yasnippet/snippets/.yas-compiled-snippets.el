;;; Compiled snippets and support files for `snippets'
;;; Snippet definitions:
;;;
(yas-define-snippets 'snippets
		     '(("@use" "(use-package $1\n  :ensure t\n   $0\n  )\n" "use-package" nil nil nil "/home/andrew/.config/emacs/etc/yasnippet/snippets/emacs-lisp-mode/use-package" nil nil)))


;;; Snippet definitions:
;;;
(yas-define-snippets 'snippets
		     '(("@test" "This is test" "test" nil nil nil "/home/andrew/.config/emacs/etc/yasnippet/snippets/fundamental-mode/test" nil nil)))


;;; Snippet definitions:
;;;
(yas-define-snippets 'snippets
		     '(("@use" "(use-package $0\n  :ensure t\n  $1\n  )\n" "use-package" nil nil nil "/home/andrew/.config/emacs/etc/yasnippet/snippets/org-mode/use-package" nil nil)
		       ("@src" "#+begin_src $0\n$1\n#+end_src\n" "src" nil nil nil "/home/andrew/.config/emacs/etc/yasnippet/snippets/org-mode/src" nil nil)
		       ("@head" "#+TITLE: $1\n#+STARTUP: $2\n#+PROPERTY: header-args:$3\n" "org-heading" nil nil nil "/home/andrew/.config/emacs/etc/yasnippet/snippets/org-mode/options" nil nil)))


;;; Snippet definitions:
;;;
(yas-define-snippets 'snippets
		     '(("blah" "Bling blargh-a bloo bloop!" "blah" nil nil nil "/home/andrew/.config/emacs/etc/yasnippet/snippets/prog-mode/blah" nil nil)))


;;; Do not edit! File generated at Thu Oct 26 18:16:06 2023
