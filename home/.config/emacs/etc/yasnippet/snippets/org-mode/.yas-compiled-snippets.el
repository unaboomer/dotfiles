;;; Compiled snippets and support files for `org-mode'
;;; Snippet definitions:
;;;
(yas-define-snippets 'org-mode
		     '(("@use" "(use-package $1\n  :ensure t\n  $0)\n" "use-package" nil nil nil "/home/andrew/.config/emacs/etc/yasnippet/snippets/org-mode/use-package" nil nil)
		       ("@src" "#+begin_src $1\n$2\n#+end_src\n" "src" nil nil nil "/home/andrew/.config/emacs/etc/yasnippet/snippets/org-mode/src" nil nil)
		       ("@orglink" "[[$1][$0]]" "Org link" nil nil nil "/home/andrew/.config/emacs/etc/yasnippet/snippets/org-mode/orglink" nil nil)
		       ("@heading" "#+TITLE: $1\n#+STARTUP: $2\n#+PROPERTY: header-args:$3\n" "org-heading" nil nil nil "/home/andrew/.config/emacs/etc/yasnippet/snippets/org-mode/options" nil nil)))


;;; Do not edit! File generated at Sat Nov 18 14:14:03 2023
