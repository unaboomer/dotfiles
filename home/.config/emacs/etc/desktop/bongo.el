(use-package bongo
  :ensure t
  :defer t
  :custom
  (bongo-enabled-backends '(mpv))
  (bongo-insert-album-covers nil)
  (bongo-insert-whole-directory-trees t)
  (bongo-logo nil)
  (bongo-display-track-icons nil)
  (bongo-display-header-icons nil)
  (bongi-display-playback-mode-indicator t)
  (bongo-display-inline-playback-progress t)
  (bongo-album-cover-size "100")
  (bongo-default-directory "~/docs/mus")
  (bongo-dired-library-mode t)
  (bongo-mode-line-paused-string "Пауза")
  (bongo-mode-line-playing-string "Играет")
  (bongo-header-line-paused-string "Пауза: ")
  (bongo-header-line-playing-string "Играет: ")
  (bongo-default-library-buffer-name "*Bongo Библиотека*")
  (bongo-default-playlist-buffer-name "*Bongo Плейлист*")
  :config
  (bongo-insert-directory-tree "~/docs/mus")
  (bongo-enable-dnd-support)
  :bind (:map bongo-mode-map
         ([remap evil-ret] . bongo-play)
         ([remap evil-forward-char] . bongo-pause/resume)
         ("C-c <right>" . bongo-play-next)
         ("C-c <left>" . bongo-play-previous)
         )
)
