(use-package exwm-modeline
  :ensure t
  :requires exwm
  :config
  (exwm-modeline-mode))

; Autostart
(vterm) ; Default frame

; Mouse follow focus
(use-package exwm-mff
  :ensure t
  :requires exwm
  :config
  (exwm-mff-mode))

(use-package exwm-systemtray
  :ensure nil
  :requires exwm
  :config
  (exwm-systemtray-enable))

(use-package exwm
  :ensure t
  :custom
  (exwm-manage-force-tiling t "force tiling mode")
  (exwm-workspace-number 9 "set default workspace-numbers")
  (exwm-systemtray-height 20 "set tray hight")
  (exwm-floating-border-width 8 "set floating border width")
  :hook (exwm-update-title-hook . (lambda () (exwm-workspace-rename-buffer exwm-title)))
  :hook (exwm-update-class-hook . (lambda () (exwm-workspace-rename-buffer exwm-class-name)))
  :hook (exwm-float-mode . (lambda () (exwm-layout-hide-modeline)))
  :custom
  ; Always pass these keys through emacs
  (exwm-input-prefix-keys
    '(?\C-x
      ?\C-u
      ?\C-h
      ?\M-x
      ?\M-`
      ?\M-&
      ?\M-:
      ?\C-\M-j ; buffer list
      ?\C-\ )) ; Ctrl+space
  (define-key exwm-mode-map [?\M-q] 'exwm-input-send-next-key)
  (exwm-input-global-keys
    '(([?\s-1] . (lambda () (interactive) (exwm-workspace-switch-create 0)))
	    ([?\s-2] . (lambda () (interactive) (exwm-workspace-switch-create 1)))
	    ([?\s-3] . (lambda () (interactive) (exwm-workspace-switch-create 2)))
	    ([?\s-4] . (lambda () (interactive) (exwm-workspace-switch-create 3)))
	    ([?\s-5] . (lambda () (interactive) (exwm-workspace-switch-create 4)))
	    ([?\s-6] . (lambda () (interactive) (exwm-workspace-switch-create 5)))
	    ([?\s-7] . (lambda () (interactive) (exwm-workspace-switch-create 6)))
	    ([?\s-8] . (lambda () (interactive) (exwm-workspace-switch-create 7)))
	    ([?\s-9] . (lambda () (interactive) (exwm-workspace-switch-create 8)))
  ))
  :bind
  ("s-w" . exwm-workspace-switch)
  ("C-s-w" . ace-window)
  ("s-r" . exwm-reset)
  ("s-h" . windmove-left)
  ("s-l" . windmove-right)
  ("s-j" . windmove-down)
  ("s-k" . windmove-up)
  ("s-d" . dmenu)
  ("s-<return>" . vterm-toggle)
  ("s-f" . exwm-layout-toggle-fullscreen)
  ("s-t" . exwm-floating-toggle-floating)
  ("C-s-h" . windsize-left)
  ("C-s-j" . windsize-down)
  ("C-s-k" . windsize-up)
  ("C-s-l" . windsize-right)
  ("s-b" . exwm-workspace-move-window)
  ("s-m" . bongo-library)
  :init
  (exwm-enable))

(use-package dmenu
  :ensure t
  :after no-littering
  :custom
  (dmenu-save-file (expand-file-name "dmenu-items" no-littering-var-directory)))

(use-package desktop-environment
  :after exwm
  :ensure t
  :diminish
  :config (desktop-environment-mode)
  :custom
  (desktop-environment-screenshot-command "maim")
  (desktop-environment-screenshot-partial-command "maim -s")
  (desktop-environment-screenshot-directory "~/")
  (desktop-environment-brightness-normal-increment "5%+")
  (desktop-environment-brightness-normal-decrement "5%-"))
