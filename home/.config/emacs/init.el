;  __  __          __                              ____
; / / / /__  ___ _/ /  ___  ___  __ _  ___ ____   / __/_ _  ___ ________
;/ /_/ / _ \/ _ `/ _ \/ _ \/ _ \/  ' \/ -_) __/  / _//  ' \/ _ `/ __(_-<
;\____/_//_/\_,_/_.__/\___/\___/_/_/_/\__/_/    /___/_/_/_/\_,_/\__/___/

(use-package package
  :custom
  (package-user-dir "~/.config/emacs/var/elpa")
  (package-native-compile t)
  (native-comp-eln-load-path (expand-file-name "~/.config/emacs/var/eln/"))
  (package-gnupghome-dir "~/.config/emacs/var/elpa/gnupg")
  (package-archives
	'(("melpa" . "https://melpa.org/packages/")
      ("elpa" . "https://elpa.gnu.org/packages/")
      ("org" . "https://orgmode.org/elpa")
      ("nongnu" . "https://elpa.nongnu.org/nongnu/")))
  (package-enable-at-startup nil)
  :config
  (package-initialize)
  (unless package-archive-contents
   (package-refresh-contents))
  :preface
  ; Run 'package-refresh-contents' before 'package-install'
  (defun package-refresh-then-install ()
    "Refreshes packages and then calls `package-install'."
    (interactive)
    (package-refresh-contents)
    (call-interactively 'package-install))
)

(use-package no-littering
  :ensure t
  :custom
  (user-emacs-directory (expand-file-name "~/.config/emacs"))
  (custom-file (expand-file-name "init.el" user-emacs-directory))
  (no-littering-etc-directory
		(expand-file-name "etc/" user-emacs-directory))
  (no-littering-var-directory
		(expand-file-name "var/" user-emacs-directory)))

(use-package use-package-core
  :custom
  (use-package-verbose t)
  (use-package-compute-statistics t)
  (use-package-always-ensure t))

(use-package auto-package-update
  :ensure t
  :after no-littering
  :defer t
  :custom
  (auto-package-update-last-update-day-path (expand-file-name "auto-update/auto-package-update-last-update-day") no-littering-var-directory)
  (auto-package-update-interval 7)
  (auto-package-update-prompt-before-update t)
  (auto-package-update-hide-results t)
  :config
  (auto-package-update-maybe)
  (auto-package-update-at-time "18:00"))

(use-package use-package-chords
  :ensure t
  :config
  (key-chord-mode 1))

(use-package quelpa
  :ensure t
  :after no-littering
  :defer t
  :custom
  (quelpa-dir (expand-file-name "quelpa/" no-littering-var-directory)))

(use-package quelpa-use-package
  :ensure t
  :defer t)

(use-package mule
  :ensure nil
  :diminish
  :config
  (prefer-coding-system 'utf-8)
  (set-terminal-coding-system 'utf-8)
  (set-language-environment "UTF-8")
  (set-keyboard-coding-system 'utf-8))

(use-package files
  :ensure nil
  :after no-littering
  :custom
  (require-final-newline nil)
  ; Backup settings
  (backup-by-copying t)
  (backup-directory-alist
   `(("." . ,(expand-file-name "backups" no-littering-var-directory))))
  ; Auto save files
  (make-directory (expand-file-name "auto-saves" no-littering-var-directory) t)
  (auto-save-list-file-prefix (expand-file-name "auto-saves/sessions" no-littering-var-directory))
  (auto-save-file-name-transform '((".*" ,(expand-file-name "auto-saves" no-littering-var-directory) t)))
  (savehist-file (expand-file-name "history" no-littering-var-directory))
  (create-lockfiles nil)
  (delete-old-versions t)
  (kept-new-versions 6)
  (kept-old-versions 2)
  (version-control t))

(use-package async-backup
  :ensure t
  :after async
  :defer t
  :hook (after-save-hook . async-backup))

(use-package gcmh
  :ensure t
  :diminish
  :custom
  (gc-cons-treshold 100000000)
  :config
  (gcmh-mode 1))

(use-package default-font-presets
  :ensure t
  :custom
  (default-font-presets-list
    (list
      "Terminess Nerd Font:style=Regular 13"
      "Iosevka NFM Light:style=medium,regular 12"
      "DejaVu Serif 11"))
  :bind ("C-c f s" . default-font-presets-choose))

(use-package frame
  :ensure nil
  :custom-face
  (default ((t (:height 120 :font "Terminess Nerd Font:style=Regular"))))
  :config
  (add-to-list 'default-frame-alist '(font . "Terminess Nerd Font:style=Regular"))
  (set-frame-font "Terminess Nerd Font:style=Regular" nil t))

(use-package gruvbox-theme
  :config
  (load-theme 'gruvbox-dark-hard t)
  :custom-face
  (cursor ((t (:background "#ebdbb2")))))

(use-package cus-edit
  :defer t
  :ensure nil
  :custom
  (custom-file null-device "Don't store customizations"))

(use-package mood-line
  ;; Enable mood-line
  :disabled
  :config
  (mood-line-mode)
  ;; Use pretty Fira Code-compatible glyphs
  :custom
  (mood-line-format
      (mood-line-defformat
       :left
       (
	      ((mood-line-segment-modal--evil-fn)  . "  ")
        ((mood-line-segment-buffer-status)   . " ")
        ((mood-line-segment-buffer-name)     . " (")
	      ((mood-line-segment-cursor-position) . ") ")
        ((mood-line-segment-scroll)          . " ")
       )
       :right
       (
        ;((mood-line-segment-cursor-position)    . "  ")
        ((when (mood-line-segment-checker) "|") . "  ")
        ((mood-line-segment-checker)            . "  ")
        (mood-line-segment-major-mode)
       )
       ))
  (mood-line-glyph-alist mood-line-glyphs-fira-code)
  )

(use-package doom-modeline
  :ensure t
	:init
	(doom-modeline-mode)
  :custom-face
  (mode-line ((t (:box (:line-width (3 . 2) :color "#504945" :style flat-button) :foreground "#ebdbb2" :background "#37302f"))))
  :custom
  (doom-modeline-height 20)
  (doom-modeline-bar-width 4)
  (doom-modeline-buffer-file-name-style 'auto)
  (doom-modeline-icon nil)
  (doom-modeline-major-mode-icon nil)
  (doom-modeline-buffer-state-icon t)
  (doom-modeline-buffer-encoding nil)
  (doom-modeline-position-column-line-format '("(%l:%c)"))
  (doom-modeline-minor-modes t)
  (doom-modeline-irc nil)
  (doom-modeline-lsp nil)
  (doom-modeline-repl nil)
  (doom-modeline-time nil)
  (doom-modeline-battery nil)
  (doom-gruvbox-dark-variant t))

(use-package doom-themes
  :ensure t
  :defer t
  :custom
  (doom-themes-enable-bold t
   doom-themes-enable-italic nil)
  :init
  (load-theme 'doom-gruvbox t)
  )

(use-package emacs
  :ensure nil
  :defer t
	:custom
  (whitspace-space 'underline)
  (whitespace-tab 'a)
  (whitespace-style '(face spaces space-mark trailing tabs tab-mark))
  :bind ("C-c y y" . whitespace-mode))

(use-package tooltip
  :ensure nil
  :custom
  (tooltip-mode -1))

(use-package emacs
  :ensure nil
  :custom
  (use-dialog-box nil)
  (x-gtk-use-system-tooltips nil)
  (use-file-dialog nil)
  (scroll-bar-mode nil)
  (menu-bar-mode nil)
  (tool-bar-mode nil))

(use-package emacs
  :ensure nil
  :custom
  (column-number-mode))

(use-package emacs
  :ensure nil
  :custom
  (set-fringe-mode 0))

(use-package emacs
  :ensure nil
  :custom
  (use-short-answer t)
  :config
  (defalias 'yes-or-no-p 'y-or-n-p)
  (defalias 'buffer-menu 'ibuffer)
  (defalias 'list-buffers 'ibuffer)
  )

(use-package emacs
  :ensure nil
  :preface
  ;; Functions to enable/disable tabs
  (defun disable-tabs () (setq indent-tabs-mode nil))
  (defun enable-tabs  ()
    (local-set-key (kbd "TAB") 'tab-to-tab-stop)
    (setq indent-tabs-mode t)
    (setq tab-width 8))
  :hook
  ((prog-mode org-mode makefile-mode lisp-mode emacs-lisp-mode) . enable-tabs))

;; Making electric-indent behave sanel
(use-package smartparens
  :ensure t
  :diminish smartparens-mode
  :hook ((prog-mode text-mode) . (smartparens-global-mode show-paren-mode)))

(use-package tab-bar
  :ensure nil
  :custom
  (global-tab-line-mode t)
  (tab-line-separator "|")
  (tab-bar-new-tab-choice "*scratch*")
  (tab-line-new-button-show nil "Don't show add-new button")
  (tab-line-close-button-show nil "Don't show close button")
  :custom-face
  (tab-line ((t (:font "Terminess Nerd Font:style=Regular" :height 120 :background "ansi-color-black"
                 :foreground "gray60" :distant-foreground "gray50" :box nil))))
  (tab-line-tab ((t (:inherit 'tab-line :background "#7F7367" :foreground "#d5c4a1" :box nil))))
  (tab-line-tab-current ((t (:background "#504945" :foreground "#ebdbb2" :bold t :box t))))
  (tab-line-tab-inactive ((t (:background "#46413e" :foreground "#a89984" :box t))))
  (tab-line-highlight ((t (:background "#3c3836" :foreground "ansi-color-white")))))

(use-package emacs
  :ensure nil
  :bind
  ("C-=" . hydra-text-scale/body)
  ("C--" . text-scale-decrease)
  ("C-+" . text-scale-increase)
  ("C-<wheel-up>" . text-scale-increase)
  ("C-<wheel-down>" . text-scale-decrease))

(use-package emacs
  :ensure nil
  :config
  (global-auto-revert-mode t))

(use-package emacs
  :ensure nil
  :hook
  ((text-mode prog-mode) . display-line-numbers-mode)
  ((org-mode dashboard-mode telega-chat-mode telega-root-mode) . (lambda () (display-line-numbers-mode -1))))

;(load "~/.config/emacs/etc/desktop/exwm.el")

;(load "~/.config/emacs/etc/desktop/bongo.el")

(use-package alert
  :ensure t
  :defer t
  :custom
  (alert-default-style 'libnotify))

(use-package telega
  :ensure t
  :defer t
  :custom
  (telega-server-libs-prefix "/usr")
  (telega-language "ru")
  (telega-dired-dwim-target t)
  ; Icons
  ;(telega-emoji-use-images t)
  (telga-emoji-font-family "Terminess Nerd Font")
  (telega-symbol-alarm "󰞏")
  (telega-symbol-pin "󰐃")
  (telega-symbol-video-chat-active "󰕧")
  (telega-symbol-unread " ")
  (telega-symbol-play "󰐊")
  (telega-symbol-folder "󰉋 ")
  (telega-symbol-failed "")
  (telega-symbol-linked "󰌷")
  (telega-symbol-chat-list "  ") ; замени
  (telega-symbol-leave-comment "󰅺")
  (telega-symbol-forum "󰊌")
  (telega-symbol-video "󰕧")
  (telega-symbol-square "󰝤")
  (telega-symbol-invoice "󰍬")
  (telega-symbol-distance "󰣰")
  (telega-symbol-keyboard "󰌌")
  (telega-symbol-multiple-folder "󰉓")
  (telega-symbol-member "󰀄")
  (telega-symbol-star "󰓎")
  (telega-emoji-reaction-list '(list "󰔓 " "1" "❤" "2" "3" "4" "5" "6" "7" "8"
                                     "9" "😢" "🎉" "🤩" "🤮" "💩" "🙏" "👌" "🕊" "🤡"
                                     "🥱" "🥴" "😍" "🐳" "❤‍🔥" "🌚" "🌭" "💯" "🤣" "⚡"
                                     "🍌" "🏆" "💔" "🤨" "😐" "🍓" "🍾" "💋" "🖕" "😈"
                                     "😴" "😭" "🤓" "👻" "👨‍💻" "👀" "🎃" "🙈" "😇" "😨"
                                     "🤝" "✍" "🤗" "🫡" "🎅" "🎄" "󰒷" "💅" "🤪" "🗿"
                                     "🆒" "💘" "🙉" "🦄" "😘" "💊" "🙊" "😎" "👾" "🤷‍♂"
                                     "🤷" "🤷‍♀" "😡"))
  (telega-symbol-reply "󰑚")
  (telega-symbol-eye "󰈈 ")
  (telega-symbol-contact "󰛋")
  (telega-symbol-photo " ")
  (telega-symbol-location "")
  (telega-symbol-credit-card "󰿯")
  (telega-symbol-checkmark "<")
  (telega-symbol-heavy-checkmark ">")
  (telega-symbol-poll-options (list "󰝦" "󰝥"))
  (telega-symbol-dice-list (list "󱅊" "󰇊" "󰇋" "󰇌" "󰇍" "󰇎" "󰇏"))
  (telega--dice-emojis (list "󱅊" "󰇊" "󰇋" "󰇌" "󰇍" "󰇎" "󰇏"))
  (telega-symbol-attachment "󰁦")
  (telega-symbol-bell "󰂚")
  (telega-symbol-lock "  ") ; заменить???
  (telega-symbol-game "󰊗")
  (telega-symbol-audio "󰕾")
  (telega-symbol-poll "󰐟")
  (telega-symbol-flames "󰈸")
  (telega-symbol-lightning "󱐋")
  (telega-symbol-premium "󰓎")
  (telega-symbol-favorite "󰃀")
  (telega-symbol-telegram " ")
  (telega-symbol-ballout-check "󰱒")
  (telega-symbol-ballout-empty "󰄱")
  (telega-symbol-poll-multiple-options (list "󰄱" "󰱒"))
  (telega-symbol-phone "󰏲 ")
  (telega-symbol-blocked "󰂭  ")
  (telega-symbol-copyright "󰗦")
  (telega-symbol-verified " 󰞑")
  (telega-symbol-mode "<")
  (telega-symbol-draft "Черновик")
  ; Appindicator colors
  (telega-appindicator-icon-colors '((offline "#504945" "#ebdbb2" nil)
					 (online "#458588" "#ebdbb2" "#b8bb26")
					 (connecting "#b16286" "#ebdbb2" "#d79921")))
  ; Settings
  (telega-video-play-inline t)
  ; Faces
  :custom-face
  (telega-button ((t (:foreground "#ebdbb2" :background "#282828" :box t))))
  (telega-button-active ((t (:foreground "#282828" :background "#ebdbb2" :box t))))
  (telega-entity-type-code ((t (:slant oblique :weight bold :family "Terminess Nerd Font"))))
  (telega-entity-type-pre ((t (:slant oblique :weight bold))))
  ; Configuration
  :config
  (require 'telega-alert)
  (telega-notifications-mode t)
  (telega-appindicator-mode t)
  (telega-alert-mode t)
  (company-mode)
  :hook
  (telega-chat-mode-hook . company-mode)
)

(use-package popper
  :ensure t
  :config
  (popper-mode t)
  (popper-echo-mode)
  :bind
  ("C-c p c" . popper-toggle-type) ; create popper buffer
  ("C-c p t" . popper-toggle) ; toggle pop-up
  ("C-c p 0" . popper-kill-latest-popup)
  ("C-c p o" . popper-cycle) ; Cycle thru created pop-up's
  )

(use-package magit
  :ensure t
  :defer t)

(use-package super-save
  :ensure t
  :diminish
  :custom
  (super-save-auto-save-when-idle t)
  (super-save-delete-trailing-whitespace 'except-current-line)
  :hook
  ((prog-mode org-mode) . super-save-mode))

(use-package eros
  :ensure t
  :custom-face
  (eros-result-overlay ((t (:box (:line-width (1 . 1) :color "#98971a" :style flat-button) :foreground "#ebdbb2" :background "#282828"))))
  :config
  (eros-mode t))

(use-package insert-shebang
  :ensure t
  :defer t
  :custom
  (insert-shebang-track-ignored-filename
          (expand-file-name "var/insert-shebang.log" user-emacs-directory)))

(use-package immersive-translate
  :ensure t
  :custom
  (immersive-translate-backend 'trans)
  (immersive-translate-failed-message "")
  (immersive-translate-pending-message "󰚭")
  (immersive-translate-trans-source-language "en")
  (immersive-translate-trans-target-language "ru")
  ;(immersive-translate-baidu-target-language "ru")
  ;(immersive-translate-deepl-target-language "RU")
  )

(use-package yasnippet
  :ensure t
  :diminish
  :after no-littering
  :custom
  (yas-new-snippet-buffer-name "+Новый сниппет+")
  (yas-global-mode t)
  (yas-minor-mode t)
  (yas-snippet-dir (expand-file-name "yasnippet") no-littering-etc-directory))

(use-package auto-yasnippet
	:ensure t
    :after no-littering
    :diminish
	:after yasnippet
	:custom
	(aya-persist-snippet (expand-file-name "ayasnippet" no-littering-var-directory))
	:commands
	(aya-create aya-expand aya-open-line)
)

(use-package consult-yasnippet
  :ensure t
  :defer t
  :bind
  ("C-c y i" . consult-yasnippet)
  ("C-c y n" . yas-next-field)
  ("C-c y p" . yas-prev-field))

(use-package hl-line
  :ensure nil
  :defer t
  :hook
  ((prog-mode text-mode) . hl-line-mode))

(use-package paradox
  :ensure t
  :defer t
  :custom
  (paradox-automatically-star nil)
  :config
  (paradox-enable)
  )

(use-package recentf
  :after no-littering
  :custom
  (add-to-list 'recentf-exclude
		(recentf-expand-file-name no-littering-var-directory))
  (add-to-list 'recentf-exclude
		(recentf-expand-file-name no-littering-etc-directory))
  (recentf-auto-cleanup 30)
  (recentf-save-file-header ";;; Автоматически сгенерировано с 'recentf' на %s.\n")
  :config
  (recentf-mode)
  :hook
  ((prog-mode org-mode) . (lambda () (run-with-idle-timer 60 t 'recentf-save-list)))
  )

(use-package transwin
  :ensure t
  :config
  (transwin-ask 90))

(use-package helpful
  :ensure t
  :defer t
  :custom-face
  (helpful-heading ((t (:height 150 :font "Terminess Nerd Font:style=Regular"))))
  (info-title-1 ((t (:height 150 :font "Terminess Nerd Font:style=Regular"))))
  (help-key-binding ((t (:height 120 :font "Terminess Nerd Font:style=Regular" :box t :foreground "LightBlue" :background "grey19"))))
  :commands (helpful-callable helpful-variable helpful-command helpful-key)
  :bind
  ([remap describe-function] . helpful-function)
  ([remap describe-command] . helpful-command)
  ([remap describe-variable] . helpful-variable)
  ([remap describe-key] . helpful-key))

(use-package try
  :ensure t
  :defer t)

(use-package hydra
  :ensure t)

(defhydra hydra-text-scale (:timeout 30)
  "Scale text"
  ("j" text-scale-increase "plus")
  ("k" text-scale-decrease "minus")
  ("g" nil "finished" :exit t))

(use-package smart-hungry-delete
  :ensure t
  :bind (([remap backward-delete-char-untabify] . smart-hungry-delete-backward-char)
	     ([remap evil-delete-backward-char] . smart-hungry-delete-backward-char)
	     ([remap evil-delete-char] . smart-hungry-delete-forward-char))
  :config
  (smart-hungry-delete-add-default-hooks))

(use-package emacs
  :ensure nil
  :custom
  (mouse-autoselect-window t))

(use-package windsize
  :ensure t
  :bind
  ("C-c C-<left>" . windsize-left)
  ("C-c C-<right>" . windsize-right)
  ("C-c C-<up>" . windsize-up)
  ("C-c C-<down>" . windsize-down))

(use-package helm
  :ensure t
  :custom
  (helm-ff-history-buffer-name "*История helm-find-files*")
  (helm-documentation-buffer-name "*Документация Helm*")
  (helm-marked-buffer-name "*helm отмеченное*")
  (helm-help-buffer-name "*Helm Помощь*")
  (helm-su-or-sudo "doas")
  (helm-back-label "[назад]")
  (helm--action-prompt "Выберите действие: ")
  (helm-action-buffer "*действие Helm*")
  ; bookmarks
  (helm-bookmark-use-icon t)
  (helm-bookmark-show-location t)
  :bind
  ("C-c b b" . helm-bookmarks))

(use-package diminish
  :ensure t)

(use-package gnus
  :ensure nil
  :defer t
  :custom
  (gnus-nntp-server nil)
  (gnus-work-buffer "*gnus работает*")
  (gnus-article-buffer "*Статья*")
  (gnus-summary-buffer "*Резюме*")
  (gnus-group-buffer "*Группа*")
  (gnus-server-buffer "*Сервер*")
  (gnus-secondary-servers "news.gmane.io")
  (gnus-select-method '(nntp "news.gmane.io"))
  :custom-face
  (gnus-header ((t (:font "Terminess Nerd Font:style=Regular")))))

(use-package ace-window
  :ensure t
  :defer t
  :bind ("C-c a w" . ace-window))

(use-package eww
  :ensure nil
  :defer t
  :custom-face
  (variable-pitch ((t :height 120 :font "Terminess Nerd Font:style=Regular")))
  (header-line ((t (:height 120 :font "Terminess Nerd Font:style=Regular"))))
  :hook
  (eww-after-render-hook . shrface-mode)
  :custom
  (browse-url-browse-function 'eww-browse-url)
  (eww-auto-rename-buffer t)
  :bind (:map eww-mode-map
    ("C-c <left>" . eww-back-url)
    ("C-c <right>" . eww-forward-url)
    ([remap evil-find-char] . link-hint-open-link)
    ("C-c TAB" . imenu-list)))

(use-package imenu-list
  ;:defer t
  :ensure t)

(use-package helm-eww
  :ensure t)

(use-package shrface
  :ensure t
  ;:defer t
  :custom
  (shrface-href-versatile t)
  (shrface-org t)
  :config
  (shrface-basic)
  (shrface-trial)
  ;(shrface-default-keybindings)
  )

(use-package vterm-toggle
  :ensure t
  :defer t
  :custom-face
  (vterm-color-red ((t :foreground "#cc241d" :background "#cc241d")))
  (vterm-color-magenta ((t :foreground "#b16286" :background "#b16286")))
  :preface
  (defun vterm-execute-line ()
  "Insert text of current line in vterm and execute."
  (interactive)
  (require 'vterm-toggle)
  (eval-when-compile (require 'subr-x))
  (let ((command (string-trim (buffer-substring
                                (save-excursion
                                  (beginning-of-line)
                                  (point))
                                (save-excursion
                                      (end-of-line)
                                  (point))))))
  (let ((buf (current-buffer)))
      (unless (get-buffer vterm-buffer-name)
        (vterm-toggle))
    (display-buffer vterm-buffer-name t)
    (switch-to-buffer-other-window vterm-buffer-name)
    (vterm--goto-line -1)
    (message command)
    (vterm-send-string command)
    (vterm-send-return)
    (switch-to-buffer-other-window buf)
  )))
  :bind (("C-c t t" . vterm-toggle)
	     ("C-c t e" . vterm-execute-line))
  :hook (vterm-mode . evil-emacs-state)
  :hook (vterm-mode . (lambda () (set (make-local-variable 'global-hl-line-mode) nil))))

(use-package eshell
  :ensure nil
  :defer t
  :custom
  (eshell-banner-message "Добро пожаловать Оболочку Emacs Eshell\n\n")
  )

(use-package remind-bindings
  :ensure t
  :custom
  (remind-bindings-buffername "*Комбинации клавиш*")
  :hook (after-init . remind-bindings-initialise)
  :bind (("C-x h" . remind-bindings-togglebuffer) ;; Toggle buffer
         ("C-c r b" . remind-bindings-specific-mode))) ; Buffer-specific only

(use-package which-key
  :ensure t
  :diminish
  :config
  (which-key-mode t)
  :custom
  (which-key-idle-delay 0.4)
  (which-key-idle-secondary-delay 0.4))

(use-package vundo
  :ensure t
  :defer t
  :commands (vundo)
  :bind (("C-c u" . vundo)
	 :map vundo-mode-map
     ;; use `hjkl` vim-like motion, also home/end to jump around.
     ("l" . vundo-forward)
     ("<right>" . vundo-forward)
     ("h" . vundo-backward)
     ("<left>" . vundo-backward)
     ("j" . vundo-next)
     ("<down>" . vundo-next)
     ("k" . vundo-previous)
     ("<up>" . vundo-previous)
     ("q" . vundo-quit)
     ("C-g" . vundo-quit)
     ("return" . vundo-confirm))
  :custom
  ;; take less on-screen space.
  (vundo-compact-display t)
  (vundo-glyph-alist vundo-unicode-symbols))

(use-package org-indent
  :ensure nil
  :defer t
  :custom
  (org-edit-src-content-indentation 0)
  :hook (org-mode . org-indent-mode)
  :diminish)

(defun org-mode-setup ()
    (visual-line-mode 1)
    (org-indent-mode 1)
    )

(use-package org
  :ensure nil
  :hook (org-mode . (lambda () (run-with-idle-timer 120 t 'org-babel-tangle))))

(use-package org
  :diminish
  :defer t
  :custom
  (org-agenda-files (list "~/docs/org/main.org"
			  "~/docs/org/home.org"
			  "~/docs/org/school.org"))
  :hook (org-mode . org-mode-setup)
  :bind (:map org-mode-map
	 ([remap org-export-dispatch] . org-ctrl-c-ctrl-c)))

(defun org-present-enable ()
    (lambda ()
      (text-scale-set 16)
      (org-present-big)
      (org-display-inline-images)
      (org-present-hide-cursor)
      (org-present-read-only)
      (setq-local face-remapping-alist '((default (:height 1.9) variable-pitch)))))

(defun org-present-disable ()
    (lambda ()
      (org-present-small)
      (org-remove-inline-images)
      (org-present-show-cursor)
      (org-present-read-write)
      (setq-local face-remapping-alist '((default variable-pitch default)))))

(use-package org-present
  :ensure t
  :defer t
  :hook (org-present-mode-hook . org-present-enable)
	(org-present-mode-quit-hook . org-present-disable))

(use-package simple-httpd
  :ensure t
  :defer t)

(use-package uimage
  :ensure t
  :diminish
  :custom
  (org-startup-with-inline-images t)
  :hook
  (org-mode . uimage-mode))

(use-package org-timer
  :ensure nil
  :custom
  (org-clock-sound "org/pomodoro.wav" no-littering-etc-directory)
  :bind
  ("C-c t s" . org-timer-set-timer)
  ("C-c t p" . org-timer-pause-or-continue))

(use-package ob-async
  :ensure t)

(use-package org
  :ensure nil
  :custom-face
  (org-document-title ((t (:height 150 :font "Terminess Nerd Font:style=Regular"))))
  (org-block-begin-line ((t (:box (:line-width (2 . 2) :color "#504945" :style flat-button) :foreground "#ebdbb2" :background "#37302f"))))
  (org-block-end-line ((t (:box (:line-width (2 . 2) :color "#504945" :style flat-button) :foreground "#ebdbb2" :background "#37302f"))))
)

(use-package toc-org
  :defer t
  :hook (org-mode-hook . toc-org-enable))

(use-package org-download
  :ensure t
  :defer t
  :hook
  (org-mode . org-download-enable)
	(dired-mode-hook . org-download-enable))

(use-package org
  :ensure nil
  :custom
  (org-export-default-language "ru"))

(use-package org-superstar
  :ensure t
  :after org
  :defer t
  :custom
  (org-pretty-entities t)
  (org-superstar-leading-bullet "")
  :hook (org-mode . prettify-symbols-mode) ;; Better symbols
  :hook (org-mode . org-superstar-mode))

(use-package org
  :ensure nil
  :custom-face
  (org-ellipsis ((t (:foreground "#ebdbb2"))))
  :custom (org-ellipsis "󱨉"))

(use-package org-ref-prettify
  :ensure t
  :after org
  :hook (org-mode . org-ref-prettify-mode))

(use-package company
  :ensure t
  :diminish
  :custom
  (company-clang--error-buffer-name "*Ошибки Clang*")
  :hook
  ((prog-mode text-mode org-mode) . company-mode))

(use-package company-box ;; more icons
  :diminish
  :hook (company-mode . company-box-mode))

(use-package company-flx
  :ensure t
  :hook (company-mode . company-flx-mode))

(use-package company-fuzzy
  :ensure t
  :diminish
  :hook (company-mode . global-company-fuzzy-mode)
  :custom
  (company-fuzzy-sorting-backend 'flx
   company-fuzzy-prefix-on-top nil
   company-fuzzy-trigger-symbols '("." "->" "<" "'" "@")))

(use-package nerd-icons-completion
  :ensure t
  :hook (company-mode . nerd-icons-completion-mode))

(use-package flycheck
  :ensure t
  :diminish
  :custom
  (flycheck-error-message-buffer "*Ошибки Flycheck*")
  (flycheck-explain-error-buffer "*Объяснение ошибки Flycheck*")
  :hook ((text-mode prog-mode) . flycheck-mode))

(use-package flycheck-popup-tip
  :ensure t
  :after flycheck
  :custom
  (flycheck-popup-tip-error-prefix ">")
  :hook (flycheck . flycheck-popup-tip-mode))

(use-package ispell
  :defer t
  :custom
  (ispell-choices-buffer "*Выбор*")
  (ispell-local-dictionary-alist
   '(("russian"
      "[АБВГДЕЁЖЗИЙКЛМНОПРСТУФХЦЧШЩЬЫЪЭЮЯабвгдеёжзийклмнопрстуфхцчшщьыъэюя’A-Za-z]"
      "[^АБВГДЕЁЖЗИЙКЛМНОПРСТУФХЦЧШЩЬЫЪЭЮЯабвгдеёжзийклмнопрстуфхцчшщьыъэюя’A-Za-z]"
      "[-']"  nil ("-d" "ru_RU,en_US") nil utf-8)))
  (ispell-program-name "hunspell")
  (ispell-dictionary "russian")
  (ispell-really-aspell nil)
  (ispell-really-hunspell t)
  (ispell-encoding8-command t)
  (ispell-silently-savep t))

(use-package flyspell
  :defer t
  ;:diminish (flyspell-mode . " φ ")
  :diminish
  :hook ((text-mode org-mode-hook) . flyspell-mode)
  :custom
  (flyspell-delay 1))

(use-package flyspell-popup
  :ensure t
  :after flyspell
  :bind
  ("C-<tab>" . flyspell-popup-correct))

(use-package beacon
  :ensure t
  :diminish
  :hook ((text-mode prog-mode) . beacon-mode))

(use-package consult
  :ensure t
  :defer t
  ;; Replace bindings. Lazily loaded due by `use-package'.
  :bind (;; C-c bindings in `mode-specific-map'
  ("C-c M-x" . consult-mode-command)
  ("C-c h" . consult-history)
  ("C-c k" . consult-kmacro)
  ([remap Info-search] . consult-info)
  ;; C-x bindings in `ctl-x-map'
  ("C-x M-:" . consult-complex-command)     ;; orig. repeat-complex-command
  ("C-x b" . consult-buffer)                ;; orig. switch-to-buffer
  ("C-x 4 b" . consult-buffer-other-window) ;; orig. switch-to-buffer-other-window
  ("C-x 5 b" . consult-buffer-other-frame)  ;; orig. switch-to-buffer-other-frame
  ("C-x r b" . consult-bookmark)            ;; orig. bookmark-jump
  ("C-x p b" . consult-project-buffer)      ;; orig. project-switch-to-buffer
  ;; Custom M-# bindings for fast register access
  ("M-#" . consult-register-load)
  ("M-'" . consult-register-store)          ;; orig. abbrev-prefix-mark (unrelated)
  ("C-M-#" . consult-register)
  ;; Other custom bindings
  ("M-y" . consult-yank-pop)                ;; orig. yank-pop
  ;; M-g bindings in `goto-map'
  ("M-g e" . consult-compile-error)
  ("M-g f" . consult-flymake)               ;; Alternative: consult-flycheck
  ("M-g g" . consult-goto-line)             ;; orig. goto-line

  ("M-g o" . consult-outline)               ;; Alternative: consult-org-heading
  ("M-g m" . consult-mark)
  ("M-g k" . consult-global-mark)
  ("M-g i" . consult-imenu)
  ("M-g I" . consult-imenu-multi)
  ;; M-s bindings in `search-map'
  ("C-x C-g" . consult-recent-file)
  ;("M-s d" . consult-find)
  ("M-s D" . consult-locate)
  ;("M-s g" . consult-grep)
  ("M-s G" . consult-git-grep)
  ("M-s r" . consult-ripgrep)
  ("M-s l" . consult-line)
  ("M-s L" . consult-line-multi)
  ("M-s k" . consult-keep-lines)
  ("M-s u" . consult-focus-lines)
  ;; Isearch integration
  ("M-s e" . consult-isearch-history)
  :map isearch-mode-map
  ("M-e" . consult-isearch-history)         ;; orig. isearch-edit-string
  ("M-s e" . consult-isearch-history)       ;; orig. isearch-edit-string
  ("M-s l" . consult-line)                  ;; needed by consult-line to detect isearch
  ("M-s L" . consult-line-multi)            ;; needed by consult-line to detect isearch
  ;; Minibuffer history
  :map minibuffer-local-map
  ("M-s" . consult-history)                 ;; orig. next-matching-history-element
  ("M-r" . consult-history))                ;; orig. previous-matching-history-element

  ;; Enable automatic preview at point in the *Completions* buffer. This is
  ;; relevant when you use the default completion UI.
  :hook (completion-list-mode . consult-preview-at-point-mode)

  ;; The :init configuration is always executed (Not lazy)
  :custom
  ;; Optionally configure the register formatting. This improves the register
  ;; preview for `consult-register', `consult-register-load',
  ;; `consult-register-store' and the Emacs built-ins.
  (register-preview-delay 0.5
  register-preview-function #'consult-register-format)

  ;; Optionally tweak the register preview window.
  ;; This adds thin lines, sorting and hides the mode line of the window.
  (advice-add #'register-preview :override #'consult-register-window)

  ;; Optionally configure the narrowing key.
  (consult-narrow-key "<") ;; "C-+"

  ;; Use Consult to select xref locations with preview
  (xref-show-xrefs-function #'consult-xref
  xref-show-definitions-function #'consult-xref)

  ;; Configure other variables and modes in the :config section,
  ;; after lazily loading the package.
  :config
  (consult-customize
  consult-theme :preview-key '(:debounce 0.2 any)
  consult-ripgrep consult-git-grep consult-grep
  consult-bookmark consult-recent-file consult-xref
  consult--source-bookmark consult--source-file-register
  consult--source-recent-file consult--source-project-recent-file
  ;; :preview-key "M-."
  :preview-key '(:debounce 0.4 any)))

(use-package rainbow-mode
  :diminish
  :defer t
  :custom
  (rainbow-ansi-colors t)
  (rainbow-html-colors t)
  (rainbow-latex-colors t)
  (rainbow-r-colors t)
  (rainbow-x-colors t)
  :hook (prog-mode helpful-mode org-mode text-mode)
) ;; You must add modes where colorize did work

(use-package rainbow-delimiters
  :ensure t
  :defer t
  :hook (prog-mode . rainbow-delimiters-mode))

(use-package dashboard
  :ensure t
  :after evil
  :config
  (dashboard-setup-startup-hook)
  ;; Navigator section ;;
  (evil-define-key nil evil-normal-state-map "a" '(lambda () (interactive) (goto-line 15) (forward-char)))
  (evil-define-key nil evil-normal-state-map "S" '(lambda () (interactive) (goto-line 15) (forward-char 3)))
  (evil-define-key nil evil-normal-state-map "T" '(lambda () (interactive) (goto-line 15) (forward-char 19)))
  (evil-define-key nil evil-normal-state-map "D" '(lambda () (interactive) (goto-line 15) (forward-char 34)))
  :custom-face
  (dashboard-banner-logo-title ((t :height 1.4 :inherit default)))
  (dashboard-footer-icon-face  ((t :height 1.2 :background "#282828" :foreground "#d3869b")))
  (dashboard-navigator-telega  ((t :background "#282828" :foreground "#458588")))
  (dashboard-navigator-scratch ((t :background "#282828" :foreground "#b8bb26")))
  (dashboard-navigator-dired   ((t :background "#282828" :foreground "#d3869b")))
  (dashboard-footer-face       ((t :inherit default)))
  :custom
  (initial-buffer-choice (lambda () (get-buffer-create "*dashboard*"))) ;; Autostart dashboard
  (dashboard-set-navigator t)
  (dashboard-navigator-buttons '((
    ("󰠮" "Scratch (S)" "Open scratch buffer" (lambda (&rest _) (scratch-buffer)) dashboard-navigator-scratch "[" " |")
    ("" "Telega (T)"  "Open Telega buffer"  (lambda (&rest _) (telega)) dashboard-navigator-telega "" " |")
    ("" "Dired (D)"   "Open Dired buffer"   (lambda (&rest _) (dired "~/")) dashboard-navigator-dired "" "]")
   )))
  ;; Set the title ;;
  (dashboard-banner-logo-title "Добро пожаловать в Emacs!")
  ;; Set tty title ;;
  (dashboard-banner-ascii "Emacs")
  ;; Separator ;;
  (dashboard-page-separator "\n──────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────\n")
  ;; Set the banner
  (dashboard-startup-banner 'logo)
  ;; Value can be
  ;; - nil to display no banner
  ;; - 'official which displays the official emacs logo
  ;; - 'logo which displays an alternative emacs logo
  ;; - 1, 2 or 3 which displays one of the text banners
  ;; - "path/to/your/image.gif", "path/to/your/image.png" or "path/to/your/text.txt" which displays whatever gif/image/text you would prefer
  ;; - a cons of '("path/to/your/image.png" . "path/to/your/text.txt")
  ;; Content is not centered by default. To center, set
  (dashboard-center-content t)
  ;; Enable(t)/disable(nil) shortcut "jump" indicators for each section
  (dashboard-show-shortcuts t)
  ;; Messages
  (dashboard-footer-messages (list
    "Один из настоящих редакторов, Emacs!"
    "Кто, чёрт возьми, использует VIM? Вперёд, Evil!"
    "Свободный как свобода, бесплатный как бесплатное Пиво"
	"Счастливого кодинга!"
	"Vi Vi Vi, редактор Дьявола"
	"Добро пожаловать в церковь Emacs"
    "Пока остальные редакторы могут сохранять файлы, только Emacs может сохранить твою Душу"
    "I showed you my source code, pls respond"
	"Я люблю котят"
	"Emacs - лучшая операционная система!"
	))
  ;; Icons
  (dashboard-display-icons-p t) ;; display icons on both GUI and terminal
  (dashboard-icon-type 'nerd-icons) ;; use `nerd-icons' package
  (dashboard-set-heading-icons t) ;; Use icons in heading
  (dashboard-set-file-icons t) ;; Use icons in file names
  ;; Change Name of the items
  (dashboard-item-names '(("Recent Files:" . "Недавние файлы:")
			  ("Agenda for today:" . "Задачи на сегодня:")
			  ("Bookmarks:" . "Закладки:")
			  ("Agenda for coming week:" . "Задачи на неделю:")))
  ;; Edit list of items
  (dashboard-items '((recents . 5)
                     (bookmarks . 5)
                     ;(projects . 5)
                     ;(agenda . 5)
                     ;(registers . 5)
		    ))
  )

(use-package su
  :ensure t
  :defer t
  :config
  (su-mode))

(use-package dired
  :ensure nil
  :diminish
  :config
  (dired-async-mode)
  :custom
  ((dired-log-buffer "*Dired Лог*")
  (dired-kill-when-opening-new-dired-buffer t "Close Dired Buffers When Another is Opened")
  (dired-listing-switches "-agho --group-directories-first")
  (dired-mouse-drag-files t))
  :hook (dired-mode . evil-emacs-state)
  :bind (:map dired-mode-map
	     ("C-c <left>" . dired-up-directory))
)

(use-package dired-open
  :ensure t
  :defer t
  :after dired
  :custom
  (dired-open-extensions '(("gif" . "nsxiv")
                          ("jpg" . "nsxiv")
                          ("png" . "nsxiv")
                          ("mkv" . "mpv")
                          ("mp4" . "mpv")
			      ("mp3" . "mpv")))
)

(use-package dired-efap
  :ensure t
  :defer t
  :config
  :bind (:map dired-mode-map
	     ("r" . dired-efap)))

(use-package dired-hide-dotfiles
  :ensure t
  :defer t
  :diminish
  :bind (:map dired-mode-map
             ([remap dired-unmark-backward] . dired-hide-dotfiles-mode))
)

(use-package diredfl
  :ensure t
  :defer t
  :hook (dired-mode . diredfl-mode)
  )

(use-package disk-usage
  :ensure t
  :after dired)

(use-package async
  :ensure t
  :config
  (async-bytecomp-package-mode 1)
  :hook
  (dired . dired-async-mode)
  (image-dired . image-dired-async-mode))

(use-package nerd-icons-dired
  :ensure t
  :diminish
  :after dired
  :defer t
  :hook (dired-mode . nerd-icons-dired-mode))

(use-package image-dired+
  :ensure t
  :defer t
  :after image-dired
  :bind ("C-c d i" . image-dired))

(use-package emacs
  :ensure nil
  :custom
  (scroll-step 1)
  (scroll-conservatively 10000)
  :config
  (pixel-scroll-precision-mode 1))

(use-package vertico
  :ensure t
  :bind (:map vertico-map
	     ("C-j" . vertico-next)
	     ("C-k" . vertico-previous)
	     ("C-h" . vertico-first)
	     ("C-l" . vertico-last)
	     ("C-f" . vertico-quick-jump)
	     ("C-<up>" . vertico-first)
	     ("C-<down>" . vertico-last)
	     ("C-p" . evil-paste-before))
  :custom
  (vertico-cycle t)
  (vertico-resize nil)
  :init
  (vertico-mode))

(use-package  marginalia
  :after vertico
  :ensure t
  :custom
  (marginalia-field-width 160)
  (margianlia-annotators '(margianlia-annotators-heavy margianlia-annotators-light nil))
  :config
  (marginalia-mode))

;; Persist history over Emacs restarts. Vertico sorts by history position.
(use-package savehist
  :ensure nil
  :after no-littering
  :custom
  (savehist-file (expand-file-name "savehist.el" no-littering-var-directory))
  (save-place-file (expand-file-name "saveplace.el" no-littering-var-directory))
  :config
  (savehist-mode)
  (save-place-mode))

(use-package emacs
  :ensure nil
  ;; Do not allow the cursor in the minibuffer prompt
  :custom
  (minibuffer-prompt-properties read-only t)
  (cursor-intangible t)
  (face minibuffer-prompt)
  ;; Enable recursive minibuffers
  (enable-recursive-minibuffers t)
  (redisplay-dont-pause t)
  :config
  (toggle-scroll-bar -1)
  (blink-cursor-mode -1)
  :custom
  (column-number-mode t)
  :hook
  (minibuffer-setup-hook . cursor-intangible-mode))

(use-package zone
  :ensure nil
  ; Убрать раздражительные zone-режимы
  :custom
  (zone-programs [zone-pgm-putz-with-case zone-pgm-dissolve zone-pgm-whack-chars zone-pgm-drip zone-pgm-drip-fretfully zone-pgm-five-oclock-swan-dive zone-pgm-martini-swan-dive zone-pgm-rat-race zone-pgm-paragraph-spaz])
  :config
  (zone-when-idle 300))

(use-package orderless
  :ensure t
  :custom
  ;; Configure a custom style dispatcher (see the Consult wiki)
  ;; (setq orderless-style-dispatchers '(+orderless-consult-dispatch orderless-affix-dispatch)
  ;;       orderless-component-separator #'orderless-escapable-split-on-space)
  (completion-styles '(orderless basic)
   completion-category-defaults nil
   completion-category-overrides '((file (styles partial-completion)))))

(use-package emacs
  :ensure nil
  :custom
  ; Scratch buffer ;
  (initial-scratch-message ";; Этот буфер предназначен для текста, который не сохраняется, и для выполнения Lisp.\n;; Чтобы создать файл, откройте его с помощью \\[find-file] и введите текст в его буфер.\n;; Используйте \\[ielm] для интерактивного выполнения Elisp-кода\n")
  (recentf-menu-title "Открыть недавние")
  (recentf-arrange-by-rule-others "Другие файлы (%d)")
  (mail-header-separator "--Текст следует за этой строкой--")
  (mail-source-incoming-file-prefix "Входящее")
  (message-sending-message "Отправляю...")
  (message-courtesy-message "Следующее сообщение - это любезно предоставленная копия статьи,\nкоторая также была размещена на сайте %s.\n\n")
  (org-ref-bibtex-new-entry/docstring "Новая запись в Bibtex:")
  (parselib-json-name-field-separator " и ")
  (bookmark-end-of-version-stamp-marker "-*- End Of Bookmark File Format Version Stamp -*-\n")
  (message-form-letter-separator "\n----------следующее сообщение формы письма следует за этой строкой----------\n")
  (org-agenda-current-time-string "← сейчас ───────────────────────────────────────────────")
  (package--default-summary "Нет описания")
  (eww-restore-reload-prompt "\n\n *** Используй \\[eww-reload] чтобы перезагрузить данный буффер. ***\n")
  (gnus-next-page-line-format "%{%(Следующая страница...%)%}\n")
  (gnus-prv-page-line-format "%{%(Предыдущая страница...%)%}\n")
  (internal--top-level-message "Вернуться к верхнему уровню")
  ;(pure-space-overflow-message "")
  (org-agenda-write-buffer-name "Просмотр Агенды")
  (server-buffer " *сервер*")
  (holiday-buffer "*Праздники*")
  (calendar-buffer "*Календарь*")
  (lunar-phases-buffer "*Фаза луны*")
  (rst-toc-buffer-name "*Оглавление*")
  (bibtex-search-buffer "*Поиск BibTeX*")
  (message-buffer-name "*Сообщения*")
  (smime-details-buffer "*OpenSSL выход*")
  (bookmark-bmenu-buffer "*Список Закладок*")
  (ido-completion-buffer "*Автодополнение Ido*")
  (nmail-article-buffer " *nmail входящие*")
  (solar-sunrise-buffer "*Время Восхода/Заката*")
  (disk-usage-buffer-name "*Использование диска*")
  (org-agenda-buffer-name "*Org Агенда*")
  (org-agenda-write-buffer-name "Просмотр Агенды")
  (byte-compile-log-buffer "*Журнал Компиляции*")
  (table-cache-buffer-name "*Кэш ячеек таблицы*")
  (shell-command-buffer-name "*Вывод Команды Оболочки*")
  (shell-command-buffer-name-async "*Вывод асинхронной Команды Оболочки*")
  (org-babel-error-buffer-name "*Ошибки Org-Babel*")
)

(use-package ace-isearch
  :ensure t
  :diminish
  :custom
  (ace-isearch-use-ace-jump nil)
  :hook
  (after-init . global-ace-isearch-mode))

(use-package emacs
  :ensure nil
  :custom
  (find-program "fd")
  (grep-program "rg")
  (browse-usrl-xterm-program "st"))

(use-package ibuffer
  :defer t
  :bind ("C-c i b" . ibuffer))

(use-package nerd-icons-ibuffer
  :ensure t
  :defer t
  :custom
  ;; Whether display the icons.
  (nerd-icons-ibuffer-icon t)
  ;; Whether display the colorful icons.
  ;; It respects `nerd-icons-color-icons'.
  (nerd-icons-ibuffer-color-icon t)
  ;; The default icon size in ibuffer.
  (nerd-icons-ibuffer-icon-size 1.0)
  ;; Use human readable file size in ibuffer.
  (nerd-icons-ibuffer-human-readable-size t)
  ;; Slow Rendering
  ;; If you experience a slow down in performance when rendering multiple icons simultaneously,
  ;; you can try setting the following variable
  (inhibit-compacting-font-caches t)
  :hook (ibuffer-mode . nerd-icons-ibuffer-mode))

(use-package link-hint
  :ensure t
  :defer t
  :bind
  ("C-c l o" . link-hint-open-link)
  ("C-c l y" . link-hint-copy-link)
  :custom
  (browse-url-browser-function 'eww-browse-url "Use eww as browser"))

(use-package avy
  :ensure t
  :defer t
  :bind
  ("C-c l l" . evil-avy-goto-line)
  ("C-c l w" . evil-avy-goto-word-0))

(use-package evil
  :ensure t
  :diminish
  :bind
  ("<escape>" . keyboard-escape-quit)
  :custom
  (evil-want-keybinding nil)
  (evil-undo-system 'undo-fu)
  :config
  (evil-mode 1))

(use-package evil-collection
  :ensure t
  :diminish (evil-collection-unimpaired-mode . "")
  :custom
  (evil-want-integration t)
  :init
  (evil-collection-init))

(use-package undo-fu
  :ensure t
  :bind
  ("C-u" . undo-fu-only-redo))

(use-package general
  :ensure t)

(use-package server
  :ensure nil
  :commands server-running-p
  :preface
  (defun server-ensure-running (frame)
    (with-selected-frame frame
      (unless (server-running-p)
        (server-start))))
  :custom
  (server-buffer " *сервер*")
  :config
  (unless (server-running-p) (server-start))
  ;:hook (after-make-frame-functions . server-ensure-running)
)
